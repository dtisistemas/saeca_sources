unit rObsSatisfConv;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelObsSatisfConv = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel4: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    lbSemestreAno: TQRLabel;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText3: TQRDBText;
    QRDBRichText1: TQRDBRichText;
    QRLabel21: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel22: TQRLabel;
    QRSysData1: TQRSysData;
    qrlbTotalPaginas: TQRLabel;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRLabel24: TQRLabel;
    qrlbNomeUsuario: TQRLabel;
    QRShape32: TQRShape;
    QRImage2: TQRImage;
  private

  public

  end;

var
  relObsSatisfConv: TrelObsSatisfConv;

implementation

uses uDM;

{$R *.DFM}

end.
