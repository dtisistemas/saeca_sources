unit rSeguro;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelSeguro = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    qrlREFERENCIA: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel12: TQRLabel;
    QRExpr2: TQRExpr;
    QRLabel8: TQRLabel;
    QRLabel11: TQRLabel;
    qrlCapital: TQRLabel;
    qrlCoeficiente: TQRLabel;
    QRImage2: TQRImage;
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public

  end;

var
  relSeguro: TrelSeguro;

implementation

uses uDM;

{$R *.DFM}

procedure TrelSeguro.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if dmDeca.cdsSel_RelacaoSeguro.FieldByName ('IDADE').Value <= 13 then
  begin
    qrlCapital.Caption     := 'R$ 1.000,00';
    qrlCoeficiente.Caption := '0,1867';
  end
  else if dmDeca.cdsSel_RelacaoSeguro.FieldByName ('IDADE').Value > 13 then
  begin
    qrlCapital.Caption     := 'R$ 3.000,00';
    qrlCoeficiente.Caption := '0,2505';
  end;
end;

end.
