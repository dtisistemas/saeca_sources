unit rObservacaoAprendizagem2;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls;

type
  TrelObservacaoAprendizagem2 = class(TQuickRep)
    DetailBand1: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel14: TQRLabel;
    edMatematica: TQRRichText;
    edObservacoes: TQRRichText;
  private

  public

  end;

var
  relObservacaoAprendizagem2: TrelObservacaoAprendizagem2;

implementation

uses uDM;

{$R *.DFM}

end.
