unit rTotalizaSeguro;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelTotalizaSeguro = class(TQuickRep)
    DetailBand1: TQRBand;
    QRBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    qrlREFERENCIA: TQRLabel;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRShape2: TQRShape;
    QRLabel2: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRExpr1: TQRExpr;
    QRImage2: TQRImage;
  private

  public

  end;

var
  relTotalizaSeguro: TrelTotalizaSeguro;

implementation

uses uDM;

{$R *.DFM}

end.
