unit rTotaisUnidade;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, TeEngine, Series, TeeProcs,
  Chart, DBChart, QrTee, jpeg, Db, TeeFunci, ADODB;

type
  TrelTotaisUnidade = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRShape17: TQRShape;
    QRChart1: TQRChart;
    QRDBChart1: TQRDBChart;
    QRDBText2: TQRDBText;
    QRShape1: TQRShape;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRSysData1: TQRSysData;
    QRLabel4: TQRLabel;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    lbUnidade: TQRLabel;
    Series1: TBarSeries;
    QRImage2: TQRImage;
  private

  public

  end;

var
  relTotaisUnidade: TrelTotaisUnidade;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

end.
