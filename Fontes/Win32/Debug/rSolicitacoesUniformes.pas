unit rSolicitacoesUniformes;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelSolicitacoesUniformes = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    qrlUnidade: TQRLabel;
    qrlPeriodo: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel4: TQRLabel;
    QRDBText1: TQRDBText;
    DetailBand1: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRBand1: TQRBand;
    QRExpr1: TQRExpr;
    QRLabel5: TQRLabel;
    QRDBText6: TQRDBText;
    SummaryBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRExpr2: TQRExpr;
    QRDBText7: TQRDBText;
    QRImage2: TQRImage;
  private

  public

  end;

var
  relSolicitacoesUniformes: TrelSolicitacoesUniformes;

implementation

uses uDM;

{$R *.DFM}

end.
