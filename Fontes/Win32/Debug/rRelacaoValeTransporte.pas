unit rRelacaoValeTransporte;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelRelacaoValeTransporte = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    W: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRSysData3: TQRSysData;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    lbUnidade: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRGroup1: TQRGroup;
    QRDBText7: TQRDBText;
    QRLabel7: TQRLabel;
    PageFooterBand1: TQRBand;
    QRShape2: TQRShape;
    QRShape12: TQRShape;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    SummaryBand1: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel20: TQRLabel;
    QRShape21: TQRShape;
    QRLabel6: TQRLabel;
    QRShape11: TQRShape;
    QRLabel15: TQRLabel;
    QRShape13: TQRShape;
    QRLabel9: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel10: TQRLabel;
    QRShape14: TQRShape;
    QRLabel8: TQRLabel;
    QRShape17: TQRShape;
    QRLabel11: TQRLabel;
    QRShape18: TQRShape;
    QRLabel12: TQRLabel;
    QRShape19: TQRShape;
    QRLabel13: TQRLabel;
    QRShape20: TQRShape;
    QRLabel14: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRShape22: TQRShape;
    QRLabel21: TQRLabel;
    QRShape24: TQRShape;
    QRShape6: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRShape5: TQRShape;
    QRShape23: TQRShape;
    QRDBText10: TQRDBText;
    QRImage2: TQRImage;
    procedure WBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private

  public

  end;

var
  relRelacaoValeTransporte: TrelRelacaoValeTransporte;

implementation

uses uDM, uSelecionarDadosFrequencia;

{$R *.DFM}

procedure TrelRelacaoValeTransporte.WBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //17/03/2015
  //mmDU := StrToInt(frmSelecionarDadosFrequencia.edDiasUteis.Text);
  relRelacaoValeTransporte.QRExpr1.Expression := '(cdsSel_Cadastro.num_cota_passes * cdsSel_DiasUteisCadastro.num_dias_uteis)*2';
  relRelacaoValeTransporte.QRExpr2.Expression := 'IF(cdsSel_Cadastro_Frequencia.num_faltas)=30,0,(cdsSel_Cadastro.num_cota_passes*cdsSel_DiasUteisCadastro.num_dias_uteis)*2)-';
  relRelacaoValeTransporte.QRExpr2.Expression := relRelacaoValeTransporte.QRExpr2.Expression + '( (cdsSel_Cadastro_Frequencia.num_faltas+cdsSel_Cadastro_Frequencia.num_justificadas)*(cdsSel_Cadastro.num_cota_passes*2)) )';
end;

end.
