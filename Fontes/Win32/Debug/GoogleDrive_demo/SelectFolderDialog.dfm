object SelectFolderDlg: TSelectFolderDlg
  Left = 0
  Top = 0
  Caption = 'Select destination folder'
  ClientHeight = 276
  ClientWidth = 309
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  DesignSize = (
    309
    276)
  PixelsPerInch = 96
  TextHeight = 13
  object SelectTV: TTreeView
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 221
    Height = 270
    Align = alLeft
    Anchors = [akLeft, akTop, akRight, akBottom]
    HideSelection = False
    Images = MainFrm.IconsIL
    Indent = 19
    ReadOnly = True
    TabOrder = 0
    OnDblClick = SelectTVDblClick
  end
  object CancelBtn: TButton
    Left = 229
    Top = 247
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object OKBtn: TButton
    Left = 229
    Top = 216
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    DoubleBuffered = True
    ModalResult = 1
    ParentDoubleBuffered = False
    TabOrder = 1
  end
end
