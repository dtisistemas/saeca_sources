object MainFrm: TMainFrm
  Left = 0
  Top = 0
  Caption = 'Google Drive v3 API component demo'
  ClientHeight = 442
  ClientWidth = 518
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TopPnl: TPanel
    Left = 0
    Top = 0
    Width = 518
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      518
      89)
    object ConnLbl: TLabel
      Left = 99
      Top = 63
      Width = 70
      Height = 13
      Caption = 'Not connected'
    end
    object Label1: TLabel
      Left = 8
      Top = 11
      Width = 45
      Height = 13
      Caption = 'Client ID:'
    end
    object Label2: TLabel
      Left = 8
      Top = 34
      Width = 64
      Height = 13
      Caption = 'Client secret:'
    end
    object HelpIm: TImage
      Left = 492
      Top = 8
      Width = 21
      Height = 21
      Center = True
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF61000000017352474200AECE1CE90000000467414D41
        0000B18F0BFC61050000001874455874536F667477617265005061696E742E4E
        45542076332E3336A9E7E225000003354944415478DA7D936B689B5518C77F27
        97DEB2244BD2BA4BB2AEA6D99AD8CD521848190CC1E906D33907C260BAB96FC2
        14B70F5EFAD96F03290CF5831B2A3AF74507114114E628DB1447D3D65D8AED7A
        49DA346BD2A6CDBD49DE37798FA79B0AAEE0E19C2FCF39E7779EE7F9FF8FE07F
        462AA3494D37D06A3A426A76BFD7537CFC8C783CB052D0A5ADC98CC52C907FC7
        EA7549A1546171318D6168F65DC140711DE0417A557A9C4D545528A5C1D2AA41
        5EADBA6EC2613111F0C0C6863AD3B17905D1ED4F877614FF03A8E8864CE40537
        A2926A69959047A3C55AA75AB7321A6F245DB0F0FC6E13BDDB0DFEB83B8ED361
        B37777F98B0F019982260B550B9F0F1A2CA5B39CD8ABD1B1D54583F5113C53D0
        3977A544B966E6CCCB0E7CAE2A91E1DB7475052C622A5190DE361B5FFE52E6A7
        A1226D4D69DE78C1CDF57B1ADFFF9AE6D03376DE3EBA8D812B296E8C55D9B7DB
        C63BAF3CC1506418B76B835DC4172BD26432D3FF459278AA44A55CA6C122A968
        52D55AE7E47E27C70FB4D37F31CEED99325B5C16064EFB998B8E91CB65118B19
        5D2657AABC7F618E7C49A7AAE948D57EB390ECEF11BCFB5A37E19B592EFCB840
        DD800D8D28C093E497A6989E99413C58D664265FE1ECA7D30AB0269981A104EC
        F0D4B8F8410FD7460B0C7C3BBFD66484EA98BDC9E0FC5B9D2C27A7189F18474C
        278AD2D12238FBF12413F3BAD2C5A452973CE5939C3FB38BF73E9B2332917FE4
        0B55927F93898F4EEF24121922B39C5A036465ABB391F0F5053E092791C2AADE
        17389AA1BBC3C6BD588542B98EBAADA6C6A903AD1C7B6E0B5F5FFE8E4EBF0F71
        3F9A08A89D49A7C3C1B9CB5106EF94540916F6EEACF3E19BBD2AFD383FFCB6A2
        ACACD3176AA6FFF54EA2B11883D7AEF2D2E117FB1EFA60E4EE84DCD4D68A2105
        5FFD9CE0EA7016578BE4C8BECD4ADABC726999677BEC9C3CE845D7CA5CBAF40D
        7BF6F4560E1F3AD8FCAF136F8D8C49D746BBD2D649349E65F4FE0ABA61C5696B
        20B8BD85F6CD36165269C2E130EDDBB672EAC471B1EE33DDBC35222BD51A6EA7
        03B7DBA99C6845D76B24534B8CFD39CE6C6C86507007C75E3D2AD67DA6B5319B
        4805B2B9FCE46C6C964422413E9743D375A580094FAB9B603084CFE7EDF3B77B
        7FFFE7CE5FCD2784E347BD43C20000000049454E44AE426082}
      OnClick = HelpImClick
    end
    object ConnectBtn: TButton
      Left = 8
      Top = 58
      Width = 75
      Height = 25
      Caption = 'Get content'
      Default = True
      TabOrder = 2
      OnClick = ConnectBtnClick
    end
    object AccInfoBtn: TButton
      Left = 393
      Top = 58
      Width = 120
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Account info...'
      Enabled = False
      TabOrder = 3
      OnClick = AccInfoBtnClick
    end
    object ClientIDEd: TEdit
      Left = 78
      Top = 8
      Width = 411
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnClick = ClientIDEdClick
    end
    object ClientSecretEd: TEdit
      Left = 78
      Top = 31
      Width = 435
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      PasswordChar = '*'
      TabOrder = 1
    end
  end
  object FoldersPnl: TPanel
    Left = 0
    Top = 89
    Width = 518
    Height = 321
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      518
      321)
    object FilesTV: TTreeView
      Left = 0
      Top = 0
      Width = 384
      Height = 321
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      HideSelection = False
      Images = IconsIL
      Indent = 19
      ReadOnly = True
      TabOrder = 0
      OnChange = FilesTVChange
      OnExpanding = FilesTVExpanding
    end
    object AddFolderBtn: TButton
      Left = 392
      Top = 3
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Create folder...'
      Enabled = False
      TabOrder = 1
      OnClick = AddFolderBtnClick
    end
    object RenameBtn: TButton
      Left = 392
      Top = 138
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Rename...'
      Enabled = False
      TabOrder = 6
      OnClick = RenameBtnClick
    end
    object MoveBtn: TButton
      Left = 392
      Top = 165
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Move...'
      Enabled = False
      TabOrder = 7
      OnClick = MoveBtnClick
    end
    object RemoveBtn: TButton
      Left = 392
      Top = 219
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Remove'
      Enabled = False
      TabOrder = 9
      OnClick = RemoveBtnClick
    end
    object UploadBtn: TBitBtn
      Left = 392
      Top = 30
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Upload file...'
      Enabled = False
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
        0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Layout = blGlyphRight
      Spacing = 5
      TabOrder = 2
      OnClick = UploadBtnClick
    end
    object DownloadBtn: TButton
      Left = 392
      Top = 84
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Download file...'
      Enabled = False
      TabOrder = 4
      OnClick = DownloadBtnClick
    end
    object FileInfoBtn: TButton
      Left = 393
      Top = 246
      Width = 120
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'File/folder info...'
      Enabled = False
      TabOrder = 10
      OnClick = FileInfoBtnClick
    end
    object DateResetBtn: TButton
      Left = 393
      Top = 273
      Width = 120
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Reset modified date'
      Enabled = False
      TabOrder = 11
      OnClick = DateResetBtnClick
    end
    object CopyBtn: TButton
      Left = 392
      Top = 192
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Copy...'
      Enabled = False
      TabOrder = 8
      OnClick = CopyBtnClick
    end
    object UpdateFileBtn: TButton
      Left = 392
      Top = 57
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Update file...'
      Enabled = False
      TabOrder = 3
      OnClick = UpdateFileBtnClick
    end
    object ShareBtn: TButton
      Left = 392
      Top = 111
      Width = 121
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Share...'
      Enabled = False
      TabOrder = 5
      OnClick = ShareBtnClick
    end
  end
  object ProgressPnl: TPanel
    Left = 0
    Top = 410
    Width = 518
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      518
      32)
    object ProgressBar: TProgressBar
      Left = 0
      Top = 0
      Width = 384
      Height = 32
      Align = alLeft
      Anchors = [akLeft, akRight, akBottom]
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 390
      Top = 3
      Width = 122
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Cancel'
      Enabled = False
      TabOrder = 1
      OnClick = CancelBtnClick
    end
  end
  object IconsIL: TImageList
    ColorDepth = cd32Bit
    Left = 328
    Top = 104
    Bitmap = {
      494C0101040008001C0010001000FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF46C7EBFF46C6
      EAFF46C6EAFF45C5EAFF45C4E9FF45C4E9FF44C3E8FF44C2E8FF44C2E7FF44C1
      E7FF43C1E6FF44C1E7FF4FC5E7FFFFFFFFFFFFFFFFFFFFFFFFFF45C7EBFF45C6
      EAFFB4E8F6FFF5EFE8FFE2CEBBFFD8BEA4FFDDC6B0FFF6F0EAFFFAF7F4FF6BCE
      ECFF42C1E6FF43C1E7FF4EC5E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF5EFE8FFE2CEBBFFD8BEA4FFDDC6B0FFF6F0EAFFFAF7F4FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FF717171FF717171FF717171FF717171FF717171FF717171FF717171FF7171
      71FF717171FF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF47C8EBFF8FE7
      FFFF8EE6FFFF8CE6FFFF8BE6FFFF89E5FFFF88E5FFFF86E4FFFF85E4FFFF83E4
      FFFF44C1E7FF83E4FFFF44C1E7FFFFFFFFFFFFFFFFFFFFFFFFFF46C8EBFFB9F0
      FFFFE2CEBBFFB17A47FFB17A47FFB17A47FFB17A47FFB17A47FFCEAD8DFFD1F5
      FFFFA3E0F3FFB9F0FFFF66CDECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A7
      A7FFE2CEBBFFB17946FFB17946FFB17946FFB17946FFB17946FFCEAD8DFFCACA
      CAFFB9B9B9FFB0B0B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF47C8ECFF91E7
      FFFF8FE7FFFF8EE6FFFF8CE6FFFF8BE6FFFF89E5FFFF88E5FFFF86E4FFFF85E4
      FFFF44C2E7FF85E4FFFF44C2E7FFFFFFFFFFFFFFFFFFFFFFFFFF46C8ECFFC8F3
      FFFFD8BEA4FFB17A47FFB17A47FFB68552FFB17A47FFB17A47FFC49D74FFF5F6
      F1FFC7D1B3FFCED7BDFFEAEEE3FFE8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFB9B9
      B9FFD8BEA4FFB17946FFB17946FFB68551FFB17946FFB17946FFC49D73FFF5F6
      F1FFC7D1B3FFCED7BDFFEAEEE3FFE8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFDEC5ACFFBD8A58FFFAF7F3FFFFFFFFFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF47C9ECFF92E7
      FFFF91E7FFFF8FE7FFFF8EE6FFFF8CE6FFFF8BE6FFFF89E5FFFF88E5FFFF86E4
      FFFF44C2E8FF86E4FFFF44C2E8FFFFFFFFFFFFFFFFFFFFFFFFFF46C9ECFFA7EC
      FFFFF0E7DDFFB68552FFBA8C5CFFF4EDE5FFC39B71FFB17A47FFE2CEBBFFCFD7
      BDFF90A465FF90A465FF90A465FFB9C6A0FFFFFFFFFFFFFFFFFFFFFFFFFF8D8D
      8DFFF0E7DDFFB68551FFBA8C5BFFF4EDE5FFC39B70FFB17946FFE2CEBBFFCFD7
      BDFF90A464FF90A464FF90A464FFB9C6A0FFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFDEC5ACFFBD8A58FFFAF7F3FFFFFFFFFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF47C9EDFF94E8
      FFFF92E7FFFF91E7FFFF8FE7FFFF8EE6FFFF8CE6FFFF8BE6FFFF89E5FFFF88E5
      FFFF44C3E8FF88E5FFFF44C3E8FFFFFFFFFFFFFFFFFFFFFFFFFF46C9EDFF94E8
      FFFFCCF4FFFFF9F5F1FFFEFDFDFFF5EFE8FFE4F9FFFFF9F5F1FFDDE3D0FF90A4
      65FF97A96EFF90A465FF90A465FFABBA8DFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFF9F5F1FFFEFDFDFFF5EFE8FFF0E4D9FFF9F5F1FFDDE3D0FF90A4
      64FF97A96DFF90A464FF90A464FFABBA8DFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48CAEDFF96E8
      FFFF94E8FFFF92E7FFFF91E7FFFF8FE7FFFF8EE6FFFF8CE6FFFF8BE6FFFF89E5
      FFFF45C4E9FF81E1FCFF45C4E9FFFFFFFFFFFFFFFFFFFFFFFFFF47CAEDFF96E8
      FFFF94E8FFFFD2F5FFFFE2CEBBFFB17A47FFE2CEBBFFEBFBFFFFA4B483FF97A9
      6EFFEAEEE3FF9DAF77FF90A465FFD5DDC6FFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFE2CEBBFFB17946FFE2CEBBFFFFFFFFFFA4B483FF97A9
      6DFFEAEEE3FF9DAF76FF90A464FFD5DDC6FFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFD4B494FFC69B70FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48CBEEFF97E8
      FFFF96E8FFFF94E8FFFF92E7FFFF91E7FFFF8FE7FFFF8EE6FFFF8CE6FFFF8BE6
      FFFF56CCEFFF4DC8ECFF73D2EEFFFFFFFFFFFFFFFFFFFFFFFFFF47CBEEFF97E8
      FFFFA3EBFFFFF5EFE8FFB17A47FFB17A47FFB17A47FFF5EFE8FFF5F6F1FFEAEE
      E3FFF1F4ECFFF1F4ECFFF1F4ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFF5EFE8FFB17946FFB17946FFB17946FFF5EFE8FFF5F6F1FFEAEE
      E3FFF1F4ECFFF1F4ECFFF1F4ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFECDED0FFB8824CFFECDED0FFFFFFFFFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48CBEEFF99E9
      FFFF97E8FFFF96E8FFFF94E8FFFF92E7FFFF91E7FFFF8FE7FFFF8EE6FFFF8CE6
      FFFF87E4FEFF45C4E9FFF3FBFDFFFFFFFFFFFFFFFFFFFFFFFFFF47CBEEFF99E9
      FFFF97E8FFFFD4F6FFFFE2CEBBFFB17A47FFE2CEBBFFD1F5FFFFD0F5FFFFD5DD
      C6FF90A465FFD5DDC6FFFAFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFE2CEBBFFB17946FFE2CEBBFFF7F1ECFFFFFFFFFFD5DD
      C6FF90A464FFD5DDC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECDED0FFB8824CFFECDED0FFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49CCEFFF9AE9
      FFFF99E9FFFF97E8FFFF96E8FFFF94E8FFFF92E7FFFF91E7FFFF8FE7FFFF8EE6
      FFFF8CE6FFFF45C5EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48CCEFFF9AE9
      FFFF99E9FFFF97E8FFFFD4F6FFFFF5EFE8FFD2F5FFFF9FEAFFFFF1F4ECFF90A4
      65FF90A465FF90A465FFF1F4ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFFFFFFFFFF5EFE8FFF7F1ECFFC1925FFFF1F4ECFF90A4
      64FF90A464FF90A464FFF1F4ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFDEC5ACFFF1E6DBFFFAF7F3FFBD8A58FFDEC5ACFFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49CDEFFF9CEA
      FFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8FFFF92E7FFFF91E7FFFF8FE7
      FFFF8EE6FFFF46C6EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48CDEFFF9CEA
      FFFF9AE9FFFF99E9FFFF97E8FFFFA3EBFFFF94E8FFFF92E7FFFFD2F5FFFFD5DD
      C6FF90A465FFD5DDC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFDEC5ACFFF3E9E0FFFAF7F3FFBD8A55FFF1E7DDFFD5DD
      C6FF90A464FFD5DDC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFE3CDB8FFB8824CFFB8824CFFC69B70FFF6EEE7FFFFFF
      FFFFFFFFFFFF717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49CDEFFF9DEA
      FFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8FFFF92E7FFFF91E7
      FFFF8FE7FFFF46C6EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48CDEFFF9DEA
      FFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8FFFF92E7FFFFD2F5
      FFFFFFFFFFFFB4E8F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFE3CDB8FFB88249FFB88249FFC69B6DFFF6EEE7FFFFFF
      FFFFFFFFFFFFC5C5C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171FF7171
      71FF717171FF797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4ACEF0FF9FEA
      FFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8FFFF92E7
      FFFF91E7FFFF46C7EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49CEF0FF9FEA
      FFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8FFFF92E7
      FFFF91E7FFFF45C7EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6EFF6E6E
      6EFF6E6E6EFF767676FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171FFFFFF
      FFFF797979FFE4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4ACFF0FFA0EB
      FFFF9FEAFFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8
      FFFF92E7FFFF47C8EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49CFF0FFA0EB
      FFFF9FEAFFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8FFFF94E8
      FFFF92E7FFFF46C8EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6EFFFFFF
      FFFF767676FFE4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171FF7979
      79FFE4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4ACFF1FFA2EB
      FFFFA0EBFFFF9FEAFFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8
      FFFF94E8FFFF47C8ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49CFF1FFA2EB
      FFFFA0EBFFFF9FEAFFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8FFFF96E8
      FFFF94E8FFFF46C8ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6EFF7676
      76FFE4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171
      71FF717171FF717171FF717171FF717171FF717171FF717171FF797979FFE4E4
      E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4AD0F1FFA3EB
      FFFFA2EBFFFFA0EBFFFF9FEAFFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8
      FFFF96E8FFFF47C9ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49D0F1FFA3EB
      FFFFA2EBFFFFA0EBFFFF9FEAFFFF9DEAFFFF9CEAFFFF9AE9FFFF99E9FFFF97E8
      FFFF96E8FFFF46C9ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E
      6EFF6E6E6EFF6E6E6EFF6E6E6EFF6E6E6EFF6E6E6EFF6E6E6EFF767676FFE4E4
      E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4BD1F2FF4AD0
      F1FF4ACFF1FF4ACFF0FF4ACEF0FF49CDEFFF49CDEFFF49CCEFFF48CBEEFF48CB
      EEFF48CAEDFF69D3F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4AD1F2FF49D0
      F1FF49CFF1FF49CFF0FF49CEF0FF48CDEFFF48CDEFFF48CCEFFF47CBEEFF47CB
      EEFF47CAEDFF68D3F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object OD: TOpenDialog
    Left = 40
    Top = 96
  end
  object SD: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 88
    Top = 96
  end
  object DownloadPM: TPopupMenu
    Left = 224
    Top = 128
  end
  object UploadPM: TPopupMenu
    Left = 144
    Top = 136
    object Uploadasis1: TMenuItem
      Caption = 'Upload as is...'
      OnClick = Uploadasis1Click
    end
    object Converttogoogledocument1: TMenuItem
      Tag = 1
      Caption = 'Convert to Google document...'
      OnClick = Uploadasis1Click
    end
  end
end
