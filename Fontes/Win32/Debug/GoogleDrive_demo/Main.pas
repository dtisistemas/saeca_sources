unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ImgList, ComCtrls, StdCtrls,
  ExtCtrls, GoogleDrive, IniFiles, Menus, Buttons, Vcl.Imaging.pngimage;//,
//  pngimage;

type
  TMainFrm = class(TForm)
    TopPnl: TPanel;
    ConnLbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    ConnectBtn: TButton;
    AccInfoBtn: TButton;
    ClientIDEd: TEdit;
    ClientSecretEd: TEdit;
    FoldersPnl: TPanel;
    FilesTV: TTreeView;
    AddFolderBtn: TButton;
    RenameBtn: TButton;
    MoveBtn: TButton;
    RemoveBtn: TButton;
    UploadBtn: TBitBtn;
    DownloadBtn: TButton;
    FileInfoBtn: TButton;
    ProgressPnl: TPanel;
    ProgressBar: TProgressBar;
    CancelBtn: TButton;
    IconsIL: TImageList;
    OD: TOpenDialog;
    SD: TSaveDialog;
    DownloadPM: TPopupMenu;
    DateResetBtn: TButton;
    CopyBtn: TButton;
    UpdateFileBtn: TButton;
    ShareBtn: TButton;
    UploadPM: TPopupMenu;
    Uploadasis1: TMenuItem;
    Converttogoogledocument1: TMenuItem;
    HelpIm: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConnectBtnClick(Sender: TObject);
    procedure AccInfoBtnClick(Sender: TObject);
    procedure FilesTVExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FilesTVChange(Sender: TObject; Node: TTreeNode);
    procedure AddFolderBtnClick(Sender: TObject);
    procedure UploadBtnClick(Sender: TObject);
    procedure DownloadBtnClick(Sender: TObject);
    procedure RenameBtnClick(Sender: TObject);
    procedure MoveBtnClick(Sender: TObject);
    procedure RemoveBtnClick(Sender: TObject);
    procedure FileInfoBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure DateResetBtnClick(Sender: TObject);
    procedure CopyBtnClick(Sender: TObject);
    procedure UpdateFileBtnClick(Sender: TObject);
    procedure ShareBtnClick(Sender: TObject);
    procedure Uploadasis1Click(Sender: TObject);
    procedure ClientIDEdClick(Sender: TObject);
    procedure HelpImClick(Sender: TObject);
  private
    GoogleDrive: TGoogleDrive;
    FCancel: Boolean;
    IniFile: TMemIniFile;
    procedure Browse(Parent: TTreeNode = nil);
    procedure UpdateButtons;
    procedure OnProgress(Sender: TObject; Position, Total: Int64; var Cancel: Boolean);
    procedure OnDownloadMIClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  MainFrm: TMainFrm;

implementation

uses SelectFolderDialog, ShellAPI;

{$R *.dfm}

function FileSizeToString(Size: Int64): String;
begin
  if Size < 1048576 then
    if Size < 1024 then
      Result := IntToStr(Size) + ' B'
    else
      Result := FloatToStrF(Size / 1024, ffFixed, 10, 1) + ' KB'
  else if Size < 1073741824 then
    Result := FloatToStrF(Size / 1048576, ffFixed, 10, 1) + ' MB'
  else
    Result := FloatToStrF(Size / 1073741824, ffFixed, 10, 1) + ' GB'
end;

procedure TMainFrm.AccInfoBtnClick(Sender: TObject);
var
  Mess: String;
begin
  Mess := 'User name:   ' + GoogleDrive.GetUserName;
  Mess := Mess + #13#10'User email:   ' + GoogleDrive.GetUserEMail;
  Mess := Mess + #13#10'UID:   ' + GoogleDrive.GetUserID;
  Mess := Mess + #13#10'Total quota:   ' + FileSizeToString(GoogleDrive.GetQuota);
  Mess := Mess + #13#10'Total quota used:   ' + FileSizeToString(GoogleDrive.GetQuotaUsed);
  Mess := Mess + #13#10'Total quota in trash:   ' + FileSizeToString(GoogleDrive.GetQuotaUsedInTrash);
  ShowMessage(Mess);
end;

procedure TMainFrm.AddFolderBtnClick(Sender: TObject);
var
  NewFolder: TGDFileInfo;
  NewItem: TTreeNode;
  NewFolderName, ParentID: String;
begin
  if FilesTV.Selected.Parent = nil then
  begin
    if FilesTV.Selected.Data <> nil then
     ParentID := GoogleDrive.SharedDrives.Names[Integer(FilesTV.Selected.Data) - 1]
    else
     ParentID := ''
  end
  else
    ParentID := TGDFileInfo(FilesTV.Selected.Data).ID;
  NewFolderName := '';
  if InputQuery('Create folder', 'Enter new folder name:', NewFolderName) then
  begin
    NewFolder := GoogleDrive.CreateFolder(NewFolderName, ParentID);
    if NewFolder <> nil then
    begin
      NewItem := FilesTV.Items.AddChildObject(FilesTV.Selected, NewFolderName, NewFolder);
      NewItem.Parent.Expand(false);
      NewItem.ImageIndex := 1;
      NewItem.SelectedIndex := 1;
      NewItem.HasChildren := False;
      FilesTV.Select(NewItem);
    end;
  end;
end;

procedure TMainFrm.Browse(Parent: TTreeNode);
var
  FFileInfo: TGDFileInfo;
  CurNode: TTreeNode;
  ParentID: String;
begin
  if (Parent <> nil) and (Parent.Parent = nil) and (Parent.getPrevSibling <> nil) and (Parent.Data = nil) then
  begin
    if GoogleDrive.FindFirstSharedWithMe(FFileInfo) <> 0 then
      Exit;
  end
  else
  if (Parent <> nil) and (Parent.Parent = nil) and (Parent.Data <> nil) then
  begin
    if GoogleDrive.FindFirst(FFileInfo, GoogleDrive.SharedDrives.Names[Integer(Parent.Data) - 1]) <> 0 then
      Exit;
  end
  else
  begin
    if Parent = nil then
      ParentID := ''
    else
      ParentID := TGDFileInfo(Parent.Data).ID;
    if GoogleDrive.FindFirst(FFileInfo, ParentID) <> 0 then
      Exit;
  end;
  repeat
    if Parent = nil then
      CurNode := FilesTV.Items.AddChildObject(FilesTV.Items.GetFirstNode, FFileInfo.Title, FFileInfo)
    else
      CurNode := FilesTV.Items.AddChildObject(Parent, FFileInfo.Title, FFileInfo);
    if FFileInfo.IsDir then
    begin
      if FFileInfo.Shared then
      begin
        CurNode.ImageIndex := 2;
        CurNode.SelectedIndex := 2;
      end
      else
      begin
        CurNode.ImageIndex := 1;
        CurNode.SelectedIndex := 1;
      end;
      CurNode.HasChildren := True;
    end
    else
    begin
      if FFileInfo.Shared then
      begin
        CurNode.ImageIndex := 3;
        CurNode.SelectedIndex := 3;
      end
      else
      begin
        CurNode.ImageIndex := 0;
        CurNode.SelectedIndex := 0;
      end;
    end;
  until GoogleDrive.FindNext(FFileInfo) <> 0;
end;

procedure TMainFrm.CancelBtnClick(Sender: TObject);
begin
  FCancel := True;
  CancelBtn.Enabled := False;
end;

procedure TMainFrm.ClientIDEdClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar('http://www.sync-components.com/about_authorization.html'), nil, nil, 1);
end;

procedure TMainFrm.ConnectBtnClick(Sender: TObject);
var
  I: Integer;
begin
  ConnLbl.Caption := 'Please wait...';
  Application.ProcessMessages;
  GoogleDrive.ClientID := Trim(ClientIDEd.Text);
  GoogleDrive.ClientSecret := Trim(ClientSecretEd.Text);
  GoogleDrive.ClearFindCache;
  FilesTV.Items.Clear;
  with FilesTV.Items.AddChild(nil, 'My Drive') do
  begin
    ImageIndex := 1;
    SelectedIndex := 1;
  end;
  Browse;
  FilesTV.Items[0].Expanded := True;
  with FilesTV.Items.AddChild(nil, 'Shared with me') do
  begin
    ImageIndex := 2;
    SelectedIndex := 2;
    HasChildren := True;
  end;
  GoogleDrive.LoadDrives;
  for I := 0 to GoogleDrive.SharedDrives.Count - 1 do
   with FilesTV.Items.AddChildObject(nil, GoogleDrive.SharedDrives.ValueFromIndex[I], Pointer(I + 1)) do
   begin
     ImageIndex := 2;
     SelectedIndex := 2;
     HasChildren := True;
   end;
  ConnLbl.Caption := 'Connected to account of: ' + GoogleDrive.GetUserName;
  GoogleDrive.GetQuota;
  ConnectBtn.Caption := 'Refresh';
  AccInfoBtn.Enabled := True;
  ClientIDEd.Enabled := False;
  ClientSecretEd.Enabled := False;
  FilesTV.Select(FilesTV.Items.GetFirstNode);
  FilesTV.SetFocus;
end;

procedure TMainFrm.CopyBtnClick(Sender: TObject);
var
  Obj, Obj2, Obj3: TGDFileInfo;
  DestParentID: String;
  CurNode, TestNode: TTreeNode;
begin
  Obj := TGDFileInfo(FilesTV.Selected.Data);
  with TSelectFolderDlg.Create(Application) do
  try
    SelectTV.Items.Assign(FilesTV.Items);
    CurNode := SelectTV.Items.GetFirstNode;
    repeat
      TestNode := CurNode;
      CurNode := CurNode.GetNext;
      if TestNode.Data = Obj then
        TestNode.Delete;
    until CurNode = nil;
    if ShowModal = mrOk then
    begin
      Obj2 := nil;
      if SelectTV.Selected.Data = nil then
        DestParentID := ''
      else
      if SelectTV.Selected.Parent = nil then
      begin
        Obj2 := Pointer(SelectTV.Selected.Data);
        DestParentID := GoogleDrive.SharedDrives.Names[Integer(SelectTV.Selected.Data) - 1]
      end
      else
      begin
        Obj2 := TGDFileInfo(SelectTV.Selected.Data);
        DestParentID := Obj2.ID;
      end;
      Obj3 := GoogleDrive.Copy(Obj, DestParentID);
      CurNode := FilesTV.Items.GetFirstNode;
      while CurNode <> nil do
      begin
        if CurNode.Data = Obj2 then
          break;
        CurNode := CurNode.GetNext;
      end;
      if CurNode <> nil then
        FilesTV.Items.AddChildObject(CurNode, Obj3.Title, Obj3);
    end;
  finally
    Free;
  end;
end;

procedure TMainFrm.DateResetBtnClick(Sender: TObject);
begin
  GoogleDrive.SetModifiedDate(TGDFileInfo(FilesTV.Selected.Data), Now);
  ShowMessage('OK');
end;

procedure TMainFrm.RemoveBtnClick(Sender: TObject);
var
  Obj: TGDFileInfo;
  ToTrash: Boolean;
begin
  ToTrash := GetKeyState(VK_SHIFT) >= 0;
  Obj := TGDFileInfo(FilesTV.Selected.Data);
  if MessageBox(Handle, 'Are you sure you want to Remove?', 'Remove', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    GoogleDrive.Remove(Obj, ToTrash);
    FilesTV.Selected.Delete;
  end;
end;

procedure TMainFrm.DownloadBtnClick(Sender: TObject);
var
  CurFile: TGDFileInfo;
  Stream: TFileStream;
  p: TPoint;
  MI: TMenuItem;
  I: Integer;
begin
  CurFile := TGDFileInfo(FilesTV.Selected.Data);
  if CurFile.ExportFormatsCount > 0 then
  with DownloadPM do
  begin
    Items.Clear;
    if CurFile.IsDownloadable then
    begin
      MI := TMenuItem.Create(DownloadPM);// CreateMenuItem;
      MI.Caption := 'As is';
      MI.Tag := -1;
      MI.OnClick := OnDownloadMIClick;
      Items.Add(MI);
      MI := TMenuItem.Create(DownloadPM);// CreateMenuItem;
      MI.Caption := '-';
      Items.Add(MI);
    end;
    for I := 0 to CurFile.ExportFormatsCount - 1 do
    begin
      MI := TMenuItem.Create(DownloadPM);// CreateMenuItem;
      MI.Caption := CurFile.ExportFormat[I];
      MI.Tag := I;
      MI.OnClick := OnDownloadMIClick;
      Items.Add(MI);
    end;
    p.X := DownloadBtn.Left;
    p.Y := DownloadBtn.Top + DownloadBtn.Height;
    p := FoldersPnl.ClientToScreen(p);
    Popup(p.X, p.Y);
    Exit;
  end;
  SD.FileName := CurFile.Title;
  if SD.Execute then
  begin
    Stream := TFileStream.Create(SD.FileName, fmCreate);
    try
      FCancel := False;
      CancelBtn.Enabled := True;
      TopPnl.Enabled := False;
      FoldersPnl.Enabled := False;
      GoogleDrive.Download(CurFile, Stream);
      ShowMessage('Downloaded');
    finally
      CancelBtn.Enabled := False;
      ProgressBar.Position := 0;
      TopPnl.Enabled := True;
      FoldersPnl.Enabled := True;
      Stream.Free;
    end;
  end;
end;

procedure TMainFrm.FileInfoBtnClick(Sender: TObject);
var
  Mess: String;
  FI: TGDFileInfo;
begin
  FI := TGDFileInfo(FilesTV.Selected.Data);
  if FI.IsDir then
    Mess := ''
  else
  begin
    Mess := 'Filename:   ' + FI.Title;
    Mess := Mess + #13#10'Size:   ' + IntToStr(FI.Size);
  end;
  Mess := Mess + #13#10'MIME Type:  ' + FI.MIMEType;
  Mess := Mess + #13#10'Modified:   ' + DateTimeToStr(FI.Modified);
  if FI.ReadOnly then
    Mess := Mess + #13#10'ReadOnly: yes'
  else
    Mess := Mess + #13#10'ReadOnly: no';
  if FI.Shared then
    Mess := Mess + #13#10'Shared: yes'
  else
    Mess := Mess + #13#10'Shared: no';
  if FI.SharedWithMe then
    Mess := Mess + #13#10'SharedWithMe: yes'
  else
    Mess := Mess + #13#10'SharedWithMe: no';
  if FI.ShareableByMe then
    Mess := Mess + #13#10'ShareableByMe: yes'
  else
    Mess := Mess + #13#10'ShareableByMe: no';

  ShowMessage(Mess);
end;

procedure TMainFrm.FilesTVChange(Sender: TObject; Node: TTreeNode);
begin
  UpdateButtons;
end;

procedure TMainFrm.FilesTVExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.Count = 0 then
    Browse(Node);
  if Node.Count = 0 then
    Node.HasChildren := False;
end;

procedure TMainFrm.FormCreate(Sender: TObject);
begin
  GoogleDrive := TGoogleDrive.Create;
  GoogleDrive.OnUploadProgress := OnProgress;
  GoogleDrive.OnDownloadProgress := OnProgress;
  GoogleDrive.LogFileName := '_GoogleDrive.log';
  IniFile := TMemIniFile.Create('RefreshToken.ini');
  ClientIDEd.Text := IniFile.ReadString('General', 'ClientID', 'Enter your client ID here');
  ClientSecretEd.Text := IniFile.ReadString('General', 'ClientSecret', 'Enter your client secret here');
  GoogleDrive.RefreshToken := IniFile.ReadString('General', 'RefreshToken', '');
end;

procedure TMainFrm.FormDestroy(Sender: TObject);
begin
  IniFile.WriteString('General', 'ClientID', Trim(ClientIDEd.Text));
  IniFile.WriteString('General', 'ClientSecret', Trim(ClientSecretEd.Text));
  IniFile.WriteString('General', 'RefreshToken', GoogleDrive.RefreshToken);
  IniFile.UpdateFile;
  IniFile.Free;
  GoogleDrive.Free;
end;

procedure TMainFrm.HelpImClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar('http://www.sync-components.com/about_authorization.html'), nil, nil, 1);
end;

procedure TMainFrm.MoveBtnClick(Sender: TObject);
var
  Obj, Obj2: TGDFileInfo;
  DestParentID: String;
  CurNode, TestNode: TTreeNode;
begin
  Obj := TGDFileInfo(FilesTV.Selected.Data);
  with TSelectFolderDlg.Create(Application) do
  try
    SelectTV.Items.Assign(FilesTV.Items);
    CurNode := SelectTV.Items.GetFirstNode;
    repeat
      TestNode := CurNode;
      CurNode := CurNode.GetNext;
      if TestNode.Data = Obj then
        TestNode.Delete;
    until CurNode = nil;
    if ShowModal = mrOk then
    begin
      Obj2 := nil;
      if SelectTV.Selected.Data = nil then
        DestParentID := ''
      else
      if SelectTV.Selected.Parent = nil then
      begin
        Obj2 := Pointer(SelectTV.Selected.Data);
        DestParentID := GoogleDrive.SharedDrives.Names[Integer(SelectTV.Selected.Data) - 1]
      end
      else
      begin
        Obj2 := TGDFileInfo(SelectTV.Selected.Data);
        DestParentID := Obj2.ID;
      end;
      GoogleDrive.Move(Obj, DestParentID);
      CurNode := FilesTV.Items.GetFirstNode;
      while CurNode <> nil do
      begin
        if CurNode.Data = Obj2 then
          break;
        CurNode := CurNode.GetNext;
      end;
      if CurNode <> nil then
        FilesTV.Selected.MoveTo(CurNode, naAddChild);
    end;
  finally
    Free;
  end;
end;

procedure TMainFrm.OnDownloadMIClick(Sender: TObject);
var
  CurFile: TGDFileInfo;
  Stream: TFileStream;
begin
  CurFile := TGDFileInfo(FilesTV.Selected.Data);
  SD.FileName := CurFile.Title;
  if CurFile.ExportExtension[TMenuItem(Sender).Tag] <> '' then
    SD.FileName := SD.FileName + CurFile.ExportExtension[TMenuItem(Sender).Tag];
  if SD.Execute then
  begin
    Stream := TFileStream.Create(SD.FileName, fmCreate);
    try
      FCancel := False;
      CancelBtn.Enabled := True;
      TopPnl.Enabled := False;
      FoldersPnl.Enabled := False;
      if TMenuItem(Sender).Tag < 0 then
        GoogleDrive.Download(CurFile, Stream)
      else
        GoogleDrive.Download(CurFile, Stream, CurFile.ExportFormat[TMenuItem(Sender).Tag]);
      ShowMessage('Downloaded');
    finally
      CancelBtn.Enabled := False;
      ProgressBar.Position := 0;
      TopPnl.Enabled := True;
      FoldersPnl.Enabled := True;
      Stream.Free;
    end;
  end;
end;

procedure TMainFrm.OnProgress(Sender: TObject; Position, Total: Int64;
  var Cancel: Boolean);
begin
  ProgressBar.Max := Total;
  ProgressBar.Position := Position;
  Application.ProcessMessages;
  Cancel := FCancel;
end;

procedure TMainFrm.RenameBtnClick(Sender: TObject);
var
  NewName: String;
  Obj: TGDFileInfo;
begin
  Obj := TGDFileInfo(FilesTV.Selected.Data);
  NewName := Obj.Title;
  if InputQuery('Rename', 'Enter new name:', NewName) then
  begin
    GoogleDrive.Rename(Obj, NewName);
    FilesTV.Selected.Text := NewName;
  end;
end;

procedure TMainFrm.ShareBtnClick(Sender: TObject);
var
  URL: String;
begin
  with FilesTV do
  begin
    if GoogleDrive.IsSharedToAnyOne(TGDFileInfo(Selected.Data)) then
    begin
      if MessageBox(Handle, 'Item is shared. Do you want to unshare it?', 'Unsharing', MB_YESNO + MB_ICONQUESTION) = IDYES then
      begin
        GoogleDrive.Unshare(TGDFileInfo(Selected.Data));
        ShowMessage('Unshared');
      end;
    end
    else
    begin
      if MessageBox(Handle, 'Item is not shared. Do you want to share it?', 'Sharing', MB_YESNO + MB_ICONQUESTION) = IDYES then
      begin
        URL := GoogleDrive.ShareToAnyOne(TGDFileInfo(Selected.Data), False);
        if URL <> '' then
          InputBox('Shared', 'The URL of shared item is:', URL);
      end;
    end;
  end;
end;

procedure TMainFrm.UpdateButtons;
begin
  AddFolderBtn.Enabled := (FilesTV.Selected <> nil)
    and (((FilesTV.Selected.Parent = nil) and ((FilesTV.Selected.getPrevSibling = nil) or (FilesTV.Selected.Data <> nil))) or ((FilesTV.Selected.Parent <> nil) and TGDFileInfo(FilesTV.Selected.Data).IsDir and not TGDFileInfo(FilesTV.Selected.Data).ReadOnly));
  ShareBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and not (TGDFileInfo(FilesTV.Selected.Data).ReadOnly or (TGDFileInfo(FilesTV.Selected.Data).DriveID <> ''));
  RenameBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and not TGDFileInfo(FilesTV.Selected.Data).ReadOnly;
  MoveBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and not TGDFileInfo(FilesTV.Selected.Data).ReadOnly;
  CopyBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and (not TGDFileInfo(FilesTV.Selected.Data).IsDir);
  RemoveBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and ((not TGDFileInfo(FilesTV.Selected.Data).ReadOnly and not TGDFileInfo(FilesTV.Selected.Data).SharedWithMe) or (TGDFileInfo(FilesTV.Selected.Data).ParentID <> ''));
  UploadBtn.Enabled := (FilesTV.Selected <> nil)
    and (((FilesTV.Selected.Parent = nil) and ((FilesTV.Selected.getPrevSibling = nil) or (FilesTV.Selected.Data <> nil))) or ((FilesTV.Selected.Parent <> nil) and TGDFileInfo(FilesTV.Selected.Data).IsDir and not TGDFileInfo(FilesTV.Selected.Data).ReadOnly));
  DownloadBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and ((TGDFileInfo(FilesTV.Selected.Data).IsDownloadable)
    or (TGDFileInfo(FilesTV.Selected.Data).ExportFormatsCount > 0));
  UpdateFileBtn.Enabled := (FilesTV.Selected <> nil)
    and (FilesTV.Selected.Parent <> nil) and (not (TGDFileInfo(FilesTV.Selected.Data).IsDir)) and not TGDFileInfo(FilesTV.Selected.Data).ReadOnly;
  FileInfoBtn.Enabled := (FilesTV.Selected <> nil) and (FilesTV.Selected.Parent <> nil);
  DateResetBtn.Enabled := (FilesTV.Selected <> nil) and (FilesTV.Selected.Parent <> nil) and not TGDFileInfo(FilesTV.Selected.Data).ReadOnly;
end;

procedure TMainFrm.UpdateFileBtnClick(Sender: TObject);
var
  CurFile: TGDFileInfo;
  Stream: TFileStream;
begin
  CurFile := TGDFileInfo(FilesTV.Selected.Data);
  if OD.Execute then
  begin
    Stream := TFileStream.Create(OD.FileName, fmOpenRead);
    try
      FCancel := False;
      CancelBtn.Enabled := True;
      TopPnl.Enabled := False;
      FoldersPnl.Enabled := False;
      GoogleDrive.Update(CurFile, Stream, ExtractFileName(OD.FileName));
      FilesTV.Selected.Text := CurFile.Title;
      ShowMessage('Updated');
    finally
      CancelBtn.Enabled := False;
      ProgressBar.Position := 0;
      TopPnl.Enabled := True;
      FoldersPnl.Enabled := True;
      Stream.Free;
    end;
  end;
end;

procedure TMainFrm.Uploadasis1Click(Sender: TObject);
var
  NewItem: TTreeNode;
  NewFile: TGDFileInfo;
  ParentID: String;
  Stream: TFileStream;
  MI: TMenuItem;
begin
  MI := TMenuItem(Sender);
  if FilesTV.Selected.Parent = nil then
  begin
    if FilesTV.Selected.Data <> nil then
     ParentID := GoogleDrive.SharedDrives.Names[Integer(FilesTV.Selected.Data) - 1]
    else
     ParentID := ''
  end
  else
    ParentID := TGDFileInfo(FilesTV.Selected.Data).ID;
  if OD.Execute then
  begin
    Stream := TFileStream.Create(OD.FileName, fmOpenRead);
    try
      FCancel := False;
      CancelBtn.Enabled := True;
      TopPnl.Enabled := False;
      FoldersPnl.Enabled := False;
      NewFile := GoogleDrive.Upload(Stream, ExtractFileName(OD.FileName), ParentID, 0, MI.Tag = 1);
      if NewFile <> nil then
      begin
        NewItem := FilesTV.Items.AddChildObject(FilesTV.Selected, NewFile.Title, NewFile);
        NewItem.Parent.Expand(false);
        NewItem.ImageIndex := 0;
        NewItem.SelectedIndex := 0;
        FilesTV.Select(NewItem);
        ShowMessage('Uploaded');
      end;
    finally
      CancelBtn.Enabled := False;
      ProgressBar.Position := 0;
      TopPnl.Enabled := True;
      FoldersPnl.Enabled := True;
      Stream.Free;
    end;
  end;
end;

procedure TMainFrm.UploadBtnClick(Sender: TObject);
var
  p: TPoint;
begin
  with UploadPM do
  begin
    p.X := UploadBtn.Left;
    p.Y := UploadBtn.Top + UploadBtn.Height;
    p := FoldersPnl.ClientToScreen(p);
    Popup(p.X, p.Y);
    Exit;
  end;
end;

end.
