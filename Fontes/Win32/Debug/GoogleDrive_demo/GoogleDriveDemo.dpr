program GoogleDriveDemo;

uses
  Forms,
  Main in 'Main.pas' {MainFrm},
  GoogleDrive in 'GoogleDrive.pas',
  SelectFolderDialog in 'SelectFolderDialog.pas' {SelectFolderDlg};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Google Drive demo';
  Application.CreateForm(TMainFrm, MainFrm);
  Application.Run;
end.
