unit SelectFolderDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ComCtrls, Main, GoogleDrive;

type
  TSelectFolderDlg = class(TForm)
    SelectTV: TTreeView;
    CancelBtn: TButton;
    OKBtn: TButton;
    procedure FormShow(Sender: TObject);
    procedure SelectTVDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TSelectFolderDlg.FormShow(Sender: TObject);
var
  I: Integer;
begin
  for I := SelectTV.Items.Count - 1 downto 1 do
    if (SelectTV.Items[I].Data = nil) or ((SelectTV.Items[I].Parent <> nil) and not TGDFileInfo(SelectTV.Items[I].Data).IsDir) then
      SelectTV.Items.Delete(SelectTV.Items[I]);
  if SelectTV.Items.Count > 0 then
  begin
    SelectTV.Items.GetFirstNode.Expand(True);
    SelectTV.Select(SelectTV.Items.GetFirstNode);
  end;
end;

procedure TSelectFolderDlg.SelectTVDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
