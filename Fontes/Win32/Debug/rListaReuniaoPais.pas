unit rListaReuniaoPais;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, jpeg;

type
  TrelListaReuniaoPais = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    lbUnidade: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel6: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText3: TQRDBText;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRImage2: TQRImage;
    procedure QRDBText2Print(sender: TObject; var Value: String);
    procedure QRDBText1Print(sender: TObject; var Value: String);
  private

  public

  end;

var
  relListaReuniaoPais: TrelListaReuniaoPais;

implementation

uses uDM, uPrincipal;

{$R *.DFM}

procedure TrelListaReuniaoPais.QRDBText2Print(sender: TObject;
  var Value: String);
begin
//if dmDeca.cdsSel_Lista_PresencaTodos.FieldByName ('Status').AsString = 'Ativo' then QRDBText2.Font.Color := clBlack;
//if dmDeca.cdsSel_Lista_PresencaTodos.FieldByName ('Status').AsString = 'Afastado' then QRDBText2.Font.Color := clRed;
//if dmDeca.cdsSel_Lista_PresencaTodos.FieldByName ('Status').AsString = 'Suspenso' then QRDBText2.Font.Color := clBlue;
end;

procedure TrelListaReuniaoPais.QRDBText1Print(sender: TObject;
  var Value: String);
begin
if dmDeca.cdsSel_Lista_PresencaTodos.FieldByName ('Status').AsString = 'Ativo' then QRDBText1.Font.Color := clBlack;
if dmDeca.cdsSel_Lista_PresencaTodos.FieldByName ('Status').AsString = 'Afastado' then QRDBText1.Font.Color := clRed;
if dmDeca.cdsSel_Lista_PresencaTodos.FieldByName ('Status').AsString = 'Suspenso' then QRDBText1.Font.Color := clBlue;
end;

end.
