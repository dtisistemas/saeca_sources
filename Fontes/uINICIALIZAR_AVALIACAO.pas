unit uINICIALIZAR_AVALIACAO;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls;

type
  TfrmINICIALIZAR_AVALIACAO = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cmbTIPO_SELECAO: TComboBox;
    edANO_AVALIACAO: TLabeledEdit;
    Label2: TLabel;
    cmbPERIODO: TComboBox;
    Label3: TLabel;
    cmbFASE: TComboBox;
    btnINICIALIZAR: TSpeedButton;
    chkDESATIVA_OPCAOFAIXAETARIA: TCheckBox;
    lbUNIDADE: TLabel;
    cmbUNIDADE: TComboBox;
    chkTODAS_UNIDADES: TCheckBox;
    ProgressBar1: TProgressBar;
    btnFINALIZAR_PERIODO: TSpeedButton;
    procedure chkDESATIVA_OPCAOFAIXAETARIAClick(Sender: TObject);
    procedure btnINICIALIZARClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkTODAS_UNIDADESClick(Sender: TObject);
    procedure VerificarCamposObrigatorios;
    procedure btnFINALIZAR_PERIODOClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmINICIALIZAR_AVALIACAO: TfrmINICIALIZAR_AVALIACAO;
  mmOK : Boolean;
  mmID_AVALIACAO : Integer;

implementation

{$R *.dfm}

uses uAVALIACAO, uDmDeca, uPRINCIPAL, uUTIL;

procedure TfrmINICIALIZAR_AVALIACAO.chkDESATIVA_OPCAOFAIXAETARIAClick(
  Sender: TObject);
begin
  if (chkDESATIVA_OPCAOFAIXAETARIA.Checked) then
  begin
    cmbFASE.ItemIndex := -1;
    cmbFASE.Enabled   := False;
  end
  else
  begin
    cmbFASE.Enabled   := True;
  end;

end;

procedure TfrmINICIALIZAR_AVALIACAO.chkTODAS_UNIDADESClick(Sender: TObject);
begin
  if (chkTODAS_UNIDADES.Checked) then
  begin
    cmbUNIDADE.ItemIndex := -1;
    cmbUNIDADE.Enabled   := False;
  end
  else
  begin
    cmbUNIDADE.Enabled   := True;
  end;
end;

procedure TfrmINICIALIZAR_AVALIACAO.FormShow(Sender: TObject);
begin

  ProgressBar1.Position := 0;

  if vvIND_PERFIL in [2,3,6] then
  begin
    cmbUNIDADE.Visible        := False;
    chkTODAS_UNIDADES.Visible := False;
    lbUNIDADE.Visible         := False;
  end
  else
  begin
    cmbUNIDADE.Visible        := True;
    chkTODAS_UNIDADES.Visible := True;
    lbUNIDADE.Visible         := True;
  end;
end;

procedure TfrmINICIALIZAR_AVALIACAO.btnFINALIZAR_PERIODOClick(Sender: TObject);
begin
  //Finalizar Avalia��es de acordo com o per�odo selecionado em tela...

  if Application.MessageBox('Deseja finalizar o per�odo selecionado para as avalia��es ?',
                            '[SAECA] - Finalizar Per�odo',
                            MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    //Selecionar todas as avalia��es (itens) para o per�odo selecoinado
    with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
    begin
      Close;
      Params.ParamByName('@pe_id_itemavaliacao').Value := Null;
      Params.ParamByName('@pe_id_avaliacao').Value     := Null;
      Params.ParamByName('@pe_ind_fase').Value         := Null;
      Params.ParamByName('@pe_periodo').Value          := cmbPERIODO.ItemIndex;
      Open;

      if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.RecordCount > 0) then
      begin
        while not dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.eof do
        begin
          with dmDeca.cdsFINALIZA_PERIODO do
          begin
            Close;
            Params.ParamByName('@pe_ind_situacao').Value     := 2; //0-GERADA;1-AVALIADA;3-FINALIZADA
            Params.ParamByName('@pe_id_itemavaliacao').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('id_itemavaliacao').Value;
            Execute;
          end;

          //Inserir dados no LOG da Avalia��o...
          with dmDeca.cdsINS_ACOMPEVOLUCAO_LOG do
          begin
            Close;
            Params.ParamByName('@pe_dat_registro').Value   := Date();
            Params.ParamByName('@pe_cod_usuario').Value    := vvCOD_USUARIO;
            Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Avalia��o FINALIZADA em ' + DateToStr(Date()) + ' por ' + vvNOM_USUARIO;
            Execute;
          end;

          dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.Next;
        end;
      end;
    end;

    Application.MessageBox('Processo de FINALIZAR PER�ODO conclu�do!',
                           '[SAECA] - Finalizar Per�odo',
                           MB_OK + MB_ICONINFORMATION);


  end;
end;

procedure TfrmINICIALIZAR_AVALIACAO.btnINICIALIZARClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja INICIALIZAR as Avalia��es de Evolu��o para o ANO/PER�ODO informado?',
                             '[SAECA] - INICIALIZAR AVALIA��ES',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    VerificarCamposObrigatorios;

    if mmOK then
    begin

      try
        with dmDeca.cdsSEL_CADASTRO do
        begin
          Close;
          Params.ParamByName('@pe_cod_matricula').Value := Null;
          Params.ParamByName('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
          Open;

          if (dmDeca.cdsSEL_CADASTRO.RecordCount < 1) then
          begin
            Application.MessageBox('A consulta n�o retornou nenhum registro referente a Unidade atual!!' + #13 + #10 +
                                   'Favor verificar a unidade antes de executar a INICIALIZA��O DA AVALIA��O',
                                   '[SAECA] - Sistema de Avalia��o de Evolu��o',
                                   MB_OK + MB_ICONWARNING);
          end
          else
          begin

            ProgressBar1.Min := 0;
            ProgressBar1.Max := dmDeca.cdsSEL_CADASTRO.RecordCount;

            while not (dmDeca.cdsSEL_CADASTRO.eof) do
            begin

              //Consulta cada matr�cula e ano informado na tabela de AVALIACAO para verificar se existe a referida avalia��o j� lan�ada
              with dmDeca.cdsSEL_ACOMPEVOLUCAO do
              begin
                Close;
                Params.ParamByName('@pe_id_avaliacao').Value      := Null;
                //Params.ParamByName('@pe_ind_fase').Value          := Null;
                //Params.ParamByName('@pe_ind_periodo').Value       := Null;
                Params.ParamByName('@pe_num_ano').Value           := StrToInt(edANO_AVALIACAO.Text);
                Params.ParamByName('@pe_ind_situacao').Value      := Null;
                Params.ParamByName('@pe_cod_matricula').Value     := dmDeca.cdsSEL_CADASTRO.FieldByName('cod_matricula').Value;
                Params.ParamByName('@pe_cod_unidade').Value       := Null;
                Params.ParamByName('@pe_dsc_periodo').Value       := Null;
                Params.ParamByName('@pe_nom_nome').Value          := Null;
                Open;

                //Se N�O encontrou AVALIA��O insere os dados na tabela AVALIACAO
                if (dmDeca.cdsSEL_ACOMPEVOLUCAO.RecordCount < 1) then
                begin
                  with dmDeca.cdsINS_ACOMPEVOLUCAO do
                  begin
                    Close;
                    Params.ParamByName('@pe_cod_matricula').Value := dmDeca.cdsSEL_CADASTRO.FieldByName('cod_matricula').Value;
                    //Params.ParamByName('@pe_dat_cadastro').Value  := Date();
                    Params.ParamByName('@pe_ind_situacao').Value  := 0;  //GERADA
                    Params.ParamByName('@pe_num_ano').Value       := StrToInt(edANO_AVALIACAO.Text);
                    Execute;

                    //Recuperar o ID da Avalia��o lan�ada
                    with dmDeca.cdsSEL_ACOMPEVOLUCAO do
                    begin
                      Close;
                      Params.ParamByName('@pe_id_avaliacao').Value      := Null;
                      //Params.ParamByName('@pe_ind_fase').Value          := Null;
                      //Params.ParamByName('@pe_ind_periodo').Value       := Null;
                      Params.ParamByName('@pe_num_ano').Value           := StrToInt(edANO_AVALIACAO.Text);
                      Params.ParamByName('@pe_ind_situacao').Value      := Null;
                      Params.ParamByName('@pe_cod_matricula').Value     := dmDeca.cdsSEL_CADASTRO.FieldByName('cod_matricula').Value;
                      Params.ParamByName('@pe_cod_unidade').Value       := Null;
                      Params.ParamByName('@pe_dsc_periodo').Value       := Null;
                      Params.ParamByName('@pe_nom_nome').Value          := Null;
                      Open;

                      mmID_AVALIACAO := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').AsInteger;

                      //Partindo do pressuposto que n�o havia avalia��o lan�ada, aqui vamos inserir o per�odo informado em tela...
                      with dmDeca.cdsINS_ACOMPEVOLUCAO_ITENS do
                      begin
                        Close;
                        Params.ParamByName('@pe_id_avaliacao').Value        := mmID_AVALIACAO;

                        //Tratar a FASE de acordo com a FAIXA ET�RIA atual do aluno
                        case (dmDeca.cdsSEL_CADASTRO.FieldByName('idade').Value) of
                            6..7: Params.ParamByName('@pe_ind_fase').Value := 1;
                               8: Params.ParamByName('@pe_ind_fase').Value := 2;
                               9: Params.ParamByName('@pe_ind_fase').Value := 3;
                              10: Params.ParamByName('@pe_ind_fase').Value := 4;
                              11: Params.ParamByName('@pe_ind_fase').Value := 5;
                              12: Params.ParamByName('@pe_ind_fase').Value := 6;
                              13: Params.ParamByName('@pe_ind_fase').Value := 7;
                          14..15: Params.ParamByName('@pe_ind_fase').Value := 8;
                        end;

                        Params.ParamByName('@pe_ind_periodo').Value         := cmbPERIODO.ItemIndex;
                        Params.ParamByName('@pe_ind_situacao').Value        := 0; //GERADA
                        Params.ParamByName('@pe_data_avaliacao').Value      := Date();
                        Params.ParamByName('@pe_log_data').Value            := Date();
                        Params.ParamByName('@pe_convivencia1').Value        := 0;
                        Params.ParamByName('@pe_convivencia2').Value        := 0;
                        Params.ParamByName('@pe_convivencia3').Value        := 0;
                        Params.ParamByName('@pe_convivencia4').Value        := 0;
                        Params.ParamByName('@pe_fluenciacom1').Value        := 0;
                        Params.ParamByName('@pe_fluenciacom2').Value        := 0;
                        Params.ParamByName('@pe_fluenciacom3').Value        := 0;
                        Params.ParamByName('@pe_fluenciamat1').Value        := 0;
                        Params.ParamByName('@pe_fluenciamat2').Value        := 0;
                        Params.ParamByName('@pe_fluenciamat3').Value        := 0;
                        Params.ParamByName('@pe_pensamentocrit1').Value     := 0;
                        Params.ParamByName('@pe_pensamentocrit2').Value     := 0;
                        Params.ParamByName('@pe_pensamentocrit3').Value     := 0;
                        Params.ParamByName('@pe_ampliacaosaberes1').Value   := 0;
                        Params.ParamByName('@pe_ampliacaosaberes2').Value   := 0;
                        Params.ParamByName('@pe_iniciacaomundotrab1').Value := 0;
                        Params.ParamByName('@pe_iniciacaomundotrab2').Value := 0;
                        Params.ParamByName('@pe_iniciacaomundotrab3').Value := 0;
                        Params.ParamByName('@pe_servicosbasicos1').Value    := 0;
                        Params.ParamByName('@pe_servicosbasicos2').Value    := 0;
                        Params.ParamByName('@pe_servicosbasicos3').Value    := 0;
                        Params.ParamByName('@pe_partprotag1').Value         := 0;
                        Params.ParamByName('@pe_partprotag2').Value         := 0;
                        Params.ParamByName('@pe_partprotag3').Value         := 0;
                        Params.ParamByName('@pe_cod_usuario').Value         := vvCOD_USUARIO;
                        Params.ParamByName('@pe_consideracoes').Value       := '<ND>';
                        Params.ParamByName('@pe_atingiu_objetivos').Value   := 0;
                        Params.ParamByName('@pe_justificativa').Value       := '<ND>';
                        Execute;
                      end;
                    end;
                  end;
                end
                //Se encontrou uma AVALIACAO, comparar o per�odo a ser lan�ado com o per�odo de tela
                else
                begin
                  mmID_AVALIACAO := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
                  with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
                  begin
                    Close;
                    Params.ParamByName('@pe_id_itemavaliacao').Value := Null;
                    Params.ParamByName('@pe_id_avaliacao').Value     := mmID_AVALIACAO;
                    Params.ParamByName('@pe_ind_fase').Value         := Null;
                    Params.ParamByName('@pe_periodo').Value          := cmbPERIODO.ItemIndex;
                    Open;

                    //Se encontrou N�O insere e nem altera....
                    if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.RecordCount > 0) then
                    begin
                      //Nada a fazer
                    end
                    else
                    //Se n�o encontrou, insere...
                    begin
                      with dmDeca.cdsINS_ACOMPEVOLUCAO_ITENS do
                      begin
                        Close;
                        Params.ParamByName('@pe_id_avaliacao').Value        := mmID_AVALIACAO;

                        //Tratar a FASE de acordo com a FAIXA ET�RIA atual do aluno
                        case (dmDeca.cdsSEL_CADASTRO.FieldByName('idade').Value) of
                            6..7: Params.ParamByName('@pe_ind_fase').Value := 1;
                               8: Params.ParamByName('@pe_ind_fase').Value := 2;
                               9: Params.ParamByName('@pe_ind_fase').Value := 3;
                              10: Params.ParamByName('@pe_ind_fase').Value := 4;
                              11: Params.ParamByName('@pe_ind_fase').Value := 5;
                              12: Params.ParamByName('@pe_ind_fase').Value := 6;
                              13: Params.ParamByName('@pe_ind_fase').Value := 7;
                          14..15: Params.ParamByName('@pe_ind_fase').Value := 8;
                        end;

                        Params.ParamByName('@pe_ind_periodo').Value         := cmbPERIODO.ItemIndex;
                        Params.ParamByName('@pe_ind_situacao').Value        := 0; //GERADA
                        Params.ParamByName('@pe_data_avaliacao').Value      := Date();
                        Params.ParamByName('@pe_log_data').Value            := Date();
                        Params.ParamByName('@pe_convivencia1').Value        := 0;
                        Params.ParamByName('@pe_convivencia2').Value        := 0;
                        Params.ParamByName('@pe_convivencia3').Value        := 0;
                        Params.ParamByName('@pe_convivencia4').Value        := 0;
                        Params.ParamByName('@pe_fluenciacom1').Value        := 0;
                        Params.ParamByName('@pe_fluenciacom2').Value        := 0;
                        Params.ParamByName('@pe_fluenciacom3').Value        := 0;
                        Params.ParamByName('@pe_fluenciamat1').Value        := 0;
                        Params.ParamByName('@pe_fluenciamat2').Value        := 0;
                        Params.ParamByName('@pe_fluenciamat3').Value        := 0;
                        Params.ParamByName('@pe_pensamentocrit1').Value     := 0;
                        Params.ParamByName('@pe_pensamentocrit2').Value     := 0;
                        Params.ParamByName('@pe_pensamentocrit3').Value     := 0;
                        Params.ParamByName('@pe_ampliacaosaberes1').Value   := 0;
                        Params.ParamByName('@pe_ampliacaosaberes2').Value   := 0;
                        Params.ParamByName('@pe_iniciacaomundotrab1').Value := 0;
                        Params.ParamByName('@pe_iniciacaomundotrab2').Value := 0;
                        Params.ParamByName('@pe_iniciacaomundotrab3').Value := 0;
                        Params.ParamByName('@pe_servicosbasicos1').Value    := 0;
                        Params.ParamByName('@pe_servicosbasicos2').Value    := 0;
                        Params.ParamByName('@pe_servicosbasicos3').Value    := 0;
                        Params.ParamByName('@pe_partprotag1').Value         := 0;
                        Params.ParamByName('@pe_partprotag2').Value         := 0;
                        Params.ParamByName('@pe_partprotag3').Value         := 0;
                        Params.ParamByName('@pe_cod_usuario').Value         := vvCOD_USUARIO;
                        Params.ParamByName('@pe_consideracoes').Value       := '<ND>';
                        Params.ParamByName('@pe_atingiu_objetivos').Value   := 0;
                        Params.ParamByName('@pe_justificativa').Value       := '<ND>';
                        Execute;
                      end;

                      //Inserir dados no LOG da Avalia��o...
                      with dmDeca.cdsINS_ACOMPEVOLUCAO_LOG do
                      begin
                        Close;
                        Params.ParamByName('@pe_dat_registro').Value   := Date();
                        Params.ParamByName('@pe_cod_usuario').Value    := vvCOD_USUARIO;
                        Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Avalia��o INICIALIZADA em ' + DateToStr(Date()) + ' por ' + vvNOM_USUARIO;
                        Execute;
                      end;

                    end;
                  end;
                end;
              end;

              dmDeca.cdsSEL_CADASTRO.Next;
              ProgressBar1.Position := ProgressBar1.Position + 1;

            end;
          end;
        end;
      except

      end;

      ShowMessage('Acabou!!!');

    end;
  end;
end;

procedure TfrmINICIALIZAR_AVALIACAO.VerificarCamposObrigatorios;
begin
  if (cmbTIPO_SELECAO.ItemIndex < 0) and (Length(edANO_AVALIACAO.Text) <4) and (cmbPERIODO.ItemIndex < 0) then
  begin
    Application.MessageBox('H� campos que devem ser preenchidos e/ou selecionados!!' + #13 + #10 +
                           'Favor verificar antes de executar a INICIALIZA��O DA AVALIA��O',
                           '[SAECA] - Sistema de Avalia��o de Evolu��o',
                           MB_OK + MB_ICONWARNING);
    mmOK := False;
  end
  else mmOK := True;
end;

end.
