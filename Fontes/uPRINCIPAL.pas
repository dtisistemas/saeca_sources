unit uPRINCIPAL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.RibbonLunaStyleActnCtrls, Vcl.Ribbon,
  Vcl.ToolWin, Vcl.ActnMan, Vcl.ActnCtrls, System.ImageList, Vcl.ImgList,
  System.Actions, Vcl.ActnList, Vcl.ComCtrls, Vcl.RibbonObsidianStyleActnCtrls,
  Vcl.StdCtrls, Vcl.Samples.Spin, Vcl.Buttons, Data.DB, Vcl.Grids, Vcl.DBGrids;

type
  TfrmPRINCIPAL = class(TForm)
    Ribbon1: TRibbon;
    RibbonPage1: TRibbonPage;
    RibbonPage2: TRibbonPage;
    RibbonGroup2: TRibbonGroup;
    ActionManager1: TActionManager;
    actFICHA_CADASTRO: TAction;
    actAVALIACOES: TAction;
    ImageList1: TImageList;
    StatusBar1: TStatusBar;
    actINICIALIZAR_AVALIACOES: TAction;
    RibbonGroup3: TRibbonGroup;
    actENCERRAR: TAction;
    RibbonGroup1: TRibbonGroup;
    RibbonGroup4: TRibbonGroup;
    Action1: TAction;
    RibbonGroup5: TRibbonGroup;
    actRESUMO: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox7: TGroupBox;
    btnAPLICAR_FILTRO: TSpeedButton;
    GroupBox4: TGroupBox;
    GroupBox11: TGroupBox;
    btnCANCELA_FILTRO_NOME: TSpeedButton;
    edtFILTRO_NOME: TEdit;
    PageControl2: TPageControl;
    DBGrid1: TDBGrid;
    ProgressBar1: TProgressBar;
    dsSEL_ACOMPEVOLUCAO_RESUMO: TDataSource;
    edtANO: TEdit;
    procedure actFICHA_CADASTROExecute(Sender: TObject);
    procedure actAVALIACOESExecute(Sender: TObject);
    procedure actINICIALIZAR_AVALIACOESExecute(Sender: TObject);
    procedure actENCERRARExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actRESUMOExecute(Sender: TObject);
    procedure btnAPLICAR_FILTROClick(Sender: TObject);
    procedure AjustaResolucao;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPRINCIPAL: TfrmPRINCIPAL;

  vvCOD_USUARIO, vvIND_PERFIL, vvCOD_UNIDADE, vvCOD_ID_ACESSO, vvTIPO_UNIDADE : Integer;
  vvNOM_USUARIO, vvNOM_UNIDADE, vvNUM_VERSAO, vvNOM_PERFIL, vvNUM_CCUSTO, vvNOM_GESTOR, vvDSC_EMAIL : String;


implementation

{$R *.dfm}

uses uFICHA_CADASTRO, uAVALIACAO, MidasLib, uINICIALIZAR_AVALIACAO, uGERARDADOS_DRIVE, uDmDeca;

const
  ScreenWidth  : LongInt = 1024;
  ScreenHeight : LongInt = 768;

procedure TfrmPRINCIPAL.actAVALIACOESExecute(Sender: TObject);
begin
  Application.CreateForm(TfrmAVALIACAO, frmAVALIACAO);
  frmAVALIACAO.Show;
end;

procedure TfrmPRINCIPAL.actENCERRARExecute(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmPRINCIPAL.actFICHA_CADASTROExecute(Sender: TObject);
begin
  Application.CreateForm(TfrmFICHA_CADASTRO, frmFICHA_CADASTRO);
  frmFICHA_CADASTRO.ShowModal;
end;

procedure TfrmPRINCIPAL.actINICIALIZAR_AVALIACOESExecute(Sender: TObject);
begin
  Application.CreateForm(TfrmINICIALIZAR_AVALIACAO,frmINICIALIZAR_AVALIACAO);
  frmINICIALIZAR_AVALIACAO.ShowModal;
end;

procedure TfrmPRINCIPAL.Action1Execute(Sender: TObject);
begin
  Application.CreateForm(TfrmGERARDADOS_DRIVE,frmGERARDADOS_DRIVE);
  frmGERARDADOS_DRIVE.Show;
end;

procedure TfrmPRINCIPAL.actRESUMOExecute(Sender: TObject);
begin
  if PageControl1.Visible = True then
    PageControl1.Visible := False
  else PageControl1.Visible := True;
end;

procedure TfrmPRINCIPAL.AjustaResolucao;
var mensagem : String;
begin
  Scaled := True;

  if (Screen.Width <> ScreenWidth) then
  begin
    height := longint(height) * longint(screen.height) DIV ScreenHeight;
    width := longint(width) * longint(screen.width) DIV ScreenWidth;
    scaleBy(screen.width, ScreenWidth);
  end;
end;

procedure TfrmPRINCIPAL.btnAPLICAR_FILTROClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja GERAR os dados para a exibi��o do RESUMO das Avalia��es referente ao ano selecionado?',
                             '[SAECA] - Gerar dados para Resumo',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin

    //Ler todos os dados de ACOMPEVOLUCAO
    with dmDeca.cdsSEL_ACOMPEVOLUCAO do
    begin
      Close;
      Params.ParamByName('@pe_id_avaliacao').Value  := Null;
      Params.ParamByName('@pe_num_ano').Value       := StrToInt(edtANO.Text);
      Params.ParamByName('@pe_ind_situacao').Value  := Null;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
      Params.ParamByName('@pe_dsc_periodo').Value   := Null;
      Params.ParamByName('@pe_nom_nome').Value      := Null;
      Open;

      if (dmDeca.cdsSEL_ACOMPEVOLUCAO.RecordCount < 1) then
      begin

      end
      else
      begin

        //Elimina os registros da tabela de RESUMO referente ao usu�rio logado
        with dmDeca.cdsDEL_ACOMPEVOLUCAO_RESUMO DO
        begin
          Close;
          Params.ParamByName('@pe_cod_usuario').Value := vvCOD_USUARIO;
          Execute;
        end;

        //Para cada avalia��o ler seus per�odos e gravar em ACOMPEVOLUCAO_RESUMO
        while not (dmDeca.cdsSEL_ACOMPEVOLUCAO.Eof) do
        begin

          //Recuperar os dados da AVALIA��O e inserir os dados no banco
          with dmDeca.cdsINS_AcompEvolucao_Resumo do
          begin
            Close;

            dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_cod_matricula').Value        := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('cod_matricula').Value;
            dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_nom_nome').Value             := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('nom_nome').Value;
            dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_num_ano').Value              := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('num_ano').Value;

            //Consulta os dados do 1.� per�odo do ano atual
            with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
            begin
              Close;
              Params.ParamByName('@pe_id_itemavaliacao').Value   := Null;
              Params.ParamByName('@pe_id_avaliacao').Value       := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
              Params.ParamByName('@pe_ind_fase').Value           := Null;
              Params.ParamByName('@pe_periodo').Value            := 1; //Primeiro Per�odo "ABRIL" (?!)
              Open;

              if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.RecordCount > 0) then
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1').Value                   := 1;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fase').Value              := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vFASE').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_situacao').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vSTATUS_PERIODO').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia4').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA4').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciacom1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciacom2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciacom3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciamat1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciamat2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciamat3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_pensamentocrit1').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_pensamentocrit2').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_pensamentocrit3').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_ampliacaosaberes1').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_ampliacaosaberes2').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_iniciacaotrab1').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_iniciacaotrab2').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_iniciacaotrab3').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_servbas1').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_servbas2').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_servbas3').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_protag1').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_protag2').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_protag3').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_atingiuobj').Value        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ATINGIU_OBJETIVOS').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_justificativa').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('JUSTIFICATIVA').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_consideracoes').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONSIDERACOES').Value;
              end
              else
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1').Value                   := 1;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fase').Value              := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_situacao').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_convivencia4').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciacom1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciacom2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciacom3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciamat1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciamat2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_fluenciamat3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_pensamentocrit1').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_pensamentocrit2').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_pensamentocrit3').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_ampliacaosaberes1').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_ampliacaosaberes2').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_iniciacaotrab1').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_iniciacaotrab2').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_iniciacaotrab3').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_servbas1').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_servbas2').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_servbas3').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_protag1').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_protag2').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_protag3').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_atingiuobj').Value        := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_justificativa').Value     := '<ND>';
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p1_consideracoes').Value     := '<ND>';
              end;
            end;


            //Consulta os dados do 2.� per�odo do ano atual
            with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
            begin
              Close;
              Params.ParamByName('@pe_id_itemavaliacao').Value   := Null;
              Params.ParamByName('@pe_id_avaliacao').Value       := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
              Params.ParamByName('@pe_ind_fase').Value           := Null;
              Params.ParamByName('@pe_periodo').Value            := 2; //Primeiro Per�odo "ABRIL" (?!)
              Open;

              if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.RecordCount > 0) then
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2').Value                   := 2;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fase').Value              := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vFASE').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_situacao').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vSTATUS_PERIODO').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia4').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA4').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciacom1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciacom2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciacom3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciamat1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciamat2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciamat3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_pensamentocrit1').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_pensamentocrit2').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_pensamentocrit3').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_ampliacaosaberes1').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_ampliacaosaberes2').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_iniciacaotrab1').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_iniciacaotrab2').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_iniciacaotrab3').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_servbas1').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_servbas2').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_servbas3').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_protag1').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_protag2').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_protag3').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_atingiuobj').Value        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ATINGIU_OBJETIVOS').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_justificativa').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('JUSTIFICATIVA').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_consideracoes').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONSIDERACOES').Value;
              end
              else
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2').Value                   := 2;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fase').Value              := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_situacao').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_convivencia4').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciacom1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciacom2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciacom3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciamat1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciamat2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_fluenciamat3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_pensamentocrit1').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_pensamentocrit2').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_pensamentocrit3').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_ampliacaosaberes1').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_ampliacaosaberes2').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_iniciacaotrab1').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_iniciacaotrab2').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_iniciacaotrab3').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_servbas1').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_servbas2').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_servbas3').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_protag1').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_protag2').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_protag3').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_atingiuobj').Value        := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_justificativa').Value     := '<ND>';
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p2_consideracoes').Value     := '<ND>';
              end;
            end;

            //Consulta os dados do 3.� per�odo do ano atual
            with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
            begin
              Close;
              Params.ParamByName('@pe_id_itemavaliacao').Value   := Null;
              Params.ParamByName('@pe_id_avaliacao').Value       := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
              Params.ParamByName('@pe_ind_fase').Value           := Null;
              Params.ParamByName('@pe_periodo').Value            := 3; //Primeiro Per�odo "ABRIL" (?!)
              Open;

              if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.RecordCount > 0) then
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3').Value                   := 3;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fase').Value              := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vFASE').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_situacao').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vSTATUS_PERIODO').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia4').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA4').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciacom1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciacom2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciacom3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciamat1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciamat2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciamat3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_pensamentocrit1').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_pensamentocrit2').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_pensamentocrit3').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_ampliacaosaberes1').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_ampliacaosaberes2').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_iniciacaotrab1').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_iniciacaotrab2').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_iniciacaotrab3').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_servbas1').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_servbas2').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_servbas3').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_protag1').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_protag2').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_protag3').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_atingiuobj').Value        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ATINGIU_OBJETIVOS').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_justificativa').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('JUSTIFICATIVA').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_consideracoes').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONSIDERACOES').Value;
              end
              else
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3').Value                   := 3;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fase').Value              := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_situacao').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_convivencia4').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciacom1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciacom2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciacom3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciamat1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciamat2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_fluenciamat3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_pensamentocrit1').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_pensamentocrit2').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_pensamentocrit3').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_ampliacaosaberes1').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_ampliacaosaberes2').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_iniciacaotrab1').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_iniciacaotrab2').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_iniciacaotrab3').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_servbas1').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_servbas2').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_servbas3').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_protag1').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_protag2').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_protag3').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_atingiuobj').Value        := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_justificativa').Value     := '<ND>';
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p3_consideracoes').Value     := '<ND>';
              end;
            end;

            //Consulta os dados do 4.� per�odo do ano atual
            with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
            begin
              Close;
              Params.ParamByName('@pe_id_itemavaliacao').Value   := Null;
              Params.ParamByName('@pe_id_avaliacao').Value       := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
              Params.ParamByName('@pe_ind_fase').Value           := Null;
              Params.ParamByName('@pe_periodo').Value            := 4; //Primeiro Per�odo "ABRIL" (?!)
              Open;

              if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.RecordCount > 0) then
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4').Value                   := 4;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fase').Value              := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vFASE').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_situacao').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('vSTATUS_PERIODO').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia4').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONVIVENCIA4').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciacom1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciacom2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciacom3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIACOM3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciamat1').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciamat2').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciamat3').Value      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('FLUENCIAMAT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_pensamentocrit1').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_pensamentocrit2').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_pensamentocrit3').Value   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PENSAMENTOCRIT3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_ampliacaosaberes1').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_ampliacaosaberes2').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('AMPLIACAOSABERES2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_iniciacaotrab1').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_iniciacaotrab2').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_iniciacaotrab3').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('INICIACAOMUNDOTRAB3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_servbas1').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_servbas2').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_servbas3').Value          := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('SERVICOSBASICOS3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_protag1').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG1').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_protag2').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG2').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_protag3').Value           := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('PARTPROTAG3').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_atingiuobj').Value        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ATINGIU_OBJETIVOS').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_justificativa').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('JUSTIFICATIVA').Value;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_consideracoes').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('CONSIDERACOES').Value;
              end
              else
              begin
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4').Value                   := 4;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fase').Value              := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_situacao').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_convivencia4').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciacom1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciacom2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciacom3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciamat1').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciamat2').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_fluenciamat3').Value      := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_pensamentocrit1').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_pensamentocrit2').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_pensamentocrit3').Value   := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_ampliacaosaberes1').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_ampliacaosaberes2').Value := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_iniciacaotrab1').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_iniciacaotrab2').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_iniciacaotrab3').Value    := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_servbas1').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_servbas2').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_servbas3').Value          := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_protag1').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_protag2').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_protag3').Value           := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_atingiuobj').Value        := Null;
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_justificativa').Value     := '<ND>';
                dmDeca.cdsINS_AcompEvolucao_Resumo.Params.ParamByName('@pe_p4_consideracoes').Value     := '<ND>';
              end;
            end;

            dmDeca.cdsINS_ACompEvolucao_Resumo.Execute;
          end;

          dmDeca.cdsSEL_ACOMPEVOLUCAO.Next;
        end;
      end;

    end;
  end;

end;

procedure TfrmPRINCIPAL.FormCreate(Sender: TObject);
begin
  //frmPRINCIPAL.AjustaResolucao;
end;

procedure TfrmPRINCIPAL.FormShow(Sender: TObject);
begin
  PageControl1.Visible := False;
end;

end.
