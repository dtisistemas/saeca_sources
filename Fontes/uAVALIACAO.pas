unit uAVALIACAO;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, Data.DB,
  Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Vcl.CheckLst, frxClass,
  frxDBSet, Vcl.Samples.Spin;

type
  TfrmAVALIACAO = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    dbgAVALIACOES: TDBGrid;
    GroupBox1: TGroupBox;
    edMATRICULA: TLabeledEdit;
    edNOME: TLabeledEdit;
    edANO_AVALIACAO: TLabeledEdit;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    Label6: TLabel;
    Label4: TLabel;
    Bevel1: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    cmbCONVIVE1: TComboBox;
    cmbCONVIVE2: TComboBox;
    cmbCONVIVE3: TComboBox;
    Label9: TLabel;
    Label10: TLabel;
    cmbCONVIVE4: TComboBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    cmbFLUENCIACOM3: TComboBox;
    cmbFLUENCIACOM2: TComboBox;
    cmbFLUENCIACOM1: TComboBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    cmbFLUMAT3: TComboBox;
    cmbFLUMAT2: TComboBox;
    cmbFLUMAT1: TComboBox;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    cmbPENS_CRITCIENT3: TComboBox;
    cmbPENS_CRITCIENT2: TComboBox;
    cmbPENS_CRITCIENT1: TComboBox;
    Label18: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    cmbAMPLIACAO_SABERES2: TComboBox;
    cmbAMPLIACAO_SABERES1: TComboBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    cmbINICIACAO_TRAB3: TComboBox;
    cmbINICIACAO_TRAB2: TComboBox;
    cmbINICIACAO_TRAB1: TComboBox;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    cmbACESSO_SERVBASICOS3: TComboBox;
    cmbACESSO_SERVBASICOS2: TComboBox;
    cmbACESSO_SERVBASICOS1: TComboBox;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    cmbPARTIC_PROTAG3: TComboBox;
    cmbPARTIC_PROTAG2: TComboBox;
    cmbPARTIC_PROTAG1: TComboBox;
    dsSEL_CADASTRO: TDataSource;
    btnEDITAR_AVALIACAO: TSpeedButton;
    btnRESUMO_AVALIACAO: TSpeedButton;
    TabSheet5: TTabSheet;
    GroupBox3: TGroupBox;
    edNASCIMENTO: TLabeledEdit;
    lbIDADE: TLabel;
    dbgITENS_AVALIACOES: TDBGrid;
    cmbFASE: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    cmbPERIODO_AVALIACAO: TComboBox;
    Label1: TLabel;
    dsSEL_ACOMPEVOLUCAO: TDataSource;
    dsSEL_ACOMPEVOLUCAO_ITENS: TDataSource;
    GroupBox6: TGroupBox;
    rdgATINGIU_OBJETIVOS: TRadioGroup;
    meCONSIDERACOES: TMemo;
    meJUSTIFICATIVA: TMemo;
    GroupBox7: TGroupBox;
    GroupBox2: TGroupBox;
    dbgEQUIPE_AVALIADORA: TDBGrid;
    btnLANCAR_EQUIPE: TSpeedButton;
    dsSEL_ACOMPEVOLUCAO_EQUIPE: TDataSource;
    GroupBox4: TGroupBox;
    GroupBox8: TGroupBox;
    cmbFILTRO_PERIODO: TComboBox;
    btnAPLICAR_FILTRO: TSpeedButton;
    GroupBox12: TGroupBox;
    cmbFILTRO_SITUACAO_AVALIACAO: TComboBox;
    btnCANCELA_FILTRO_PERIODO_FUNDHAS: TSpeedButton;
    btnCANCELA_FILTRO_SITUACAO_AVALIACAO: TSpeedButton;
    sedFILTRO_ANO_AVALIACAO: TSpinEdit;
    GroupBox13: TGroupBox;
    GroupBox9: TGroupBox;
    btnCANCELA_FILTRO_FAIXA_ETARIA: TSpeedButton;
    cmbFILTRO_FASE_ETARIA: TComboBox;
    GroupBox11: TGroupBox;
    btnCANCELA_FILTRO_NOME: TSpeedButton;
    edtFILTRO_NOME: TEdit;
    GroupBox10: TGroupBox;
    btnCANCELA_FILTRO_AVALIACAO: TSpeedButton;
    cmbFILTRO_PERIODO_AVALIACAO: TComboBox;
    btnFILTRO_ITENS: TSpeedButton;
    btnEDITAR_PERIODO: TSpeedButton;
    btnSAIR: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure btnNOVA_AVALIACAO2Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnINICIALIZAR_AVALIACAOClick(Sender: TObject);
    procedure dbgAVALIACOESCellClick(Column: TColumn);
    procedure btnEDITAR_AVALIACAOClick(Sender: TObject);
    procedure btnEDITAR_PERIODOClick(Sender: TObject);
    procedure dbgAVALIACOESDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgITENS_AVALIACOESDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnLANCAR_EQUIPEClick(Sender: TObject);
    procedure btnAPLICAR_FILTROClick(Sender: TObject);
    procedure btnCANCELA_FILTRO_PERIODO_FUNDHASClick(Sender: TObject);
    procedure btnCANCELA_FILTRO_FAIXA_ETARIAClick(Sender: TObject);
    procedure btnCANCELA_FILTRO_AVALIACAOClick(Sender: TObject);
    procedure btnCANCELA_FILTRO_NOMEClick(Sender: TObject);
    procedure btnCANCELA_FILTRO_SITUACAO_AVALIACAOClick(Sender: TObject);
    procedure btnFILTRO_ITENSClick(Sender: TObject);
    procedure btnSAIRClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AjustaResolucaoAvaliacao;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAVALIACAO: TfrmAVALIACAO;

implementation
uses uDmDeca, uPRINCIPAL, uINICIALIZAR_AVALIACAO, uCADASTRA_EQUIPE;

const
  ScreenWidth2: LongInt = 1024; {I designed my form in 800x600 mode.}
  ScreenHeight2: LongInt = 768;

{$R *.dfm}

procedure TfrmAVALIACAO.AjustaResolucaoAvaliacao;
var mensagem : String;
begin
  Scaled := True;

  if (Screen.Width <> ScreenWidth2) then
  begin
    height := longint(height) * longint(screen.height) DIV ScreenHeight2;
    width := longint(width) * longint(screen.width) DIV ScreenWidth2;
    scaleBy(screen.width, ScreenWidth2);
  end;
end;

procedure TfrmAVALIACAO.btnAPLICAR_FILTROClick(Sender: TObject);
begin
     try
      with dmDeca.cdsSEL_ACOMPEVOLUCAO do
      begin
        Close;
        Params.ParamByName('@pe_id_avaliacao').Value := Null;
        if cmbFILTRO_PERIODO.ItemIndex = -1 then Params.ParamByName('@pe_dsc_periodo').Value := Null else Params.ParamByName('@pe_dsc_periodo').Value := Trim(cmbFILTRO_PERIODO.Text);
        //if cmbFILTRO_FASE_ETARIA.ItemIndex = -1 then Params.ParamByName('@pe_ind_fase').Value := Null else Params.ParamByName('@pe_ind_fase').Value := cmbFILTRO_FASE_ETARIA.ItemIndex+1;
        //if cmbFILTRO_PERIODO_AVALIACAO.ItemIndex = -1 then Params.ParamByName('@pe_ind_periodo').Value := Null else Params.ParamByName('@pe_ind_periodo').Value := cmbFILTRO_PERIODO_AVALIACAO.ItemIndex;
        if cmbFILTRO_SITUACAO_AVALIACAO.ItemIndex = -1 then Params.ParamByName('@pe_ind_situacao').Value := Null else Params.ParamByName('@pe_ind_situacao').Value := cmbFILTRO_SITUACAO_AVALIACAO.ItemIndex;
        Params.ParamByName('@pe_num_ano').Value       := StrToInt(sedFILTRO_ANO_AVALIACAO.Text);
        Params.ParamByName('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
        if (Length(edtFILTRO_NOME.Text)=0) then Params.ParamByName('@pe_nom_nome').Value := Null else Params.ParamByName('@pe_nom_nome').Value := Trim(edtFILTRO_NOME.Text) + '%';

        Open;
        dbgAVALIACOES.Refresh;
      end;
    except

    end;
end;

procedure TfrmAVALIACAO.btnCANCELA_FILTRO_AVALIACAOClick(Sender: TObject);
begin
  cmbFILTRO_PERIODO_AVALIACAO.ItemIndex := -1;
end;

procedure TfrmAVALIACAO.btnCANCELA_FILTRO_FAIXA_ETARIAClick(Sender: TObject);
begin
  cmbFILTRO_FASE_ETARIA.ItemIndex := -1;
end;

procedure TfrmAVALIACAO.btnCANCELA_FILTRO_NOMEClick(Sender: TObject);
begin
  edtFILTRO_NOME.Clear;
end;

procedure TfrmAVALIACAO.btnCANCELA_FILTRO_PERIODO_FUNDHASClick(Sender: TObject);
begin
  cmbFILTRO_PERIODO.ItemIndex := -1;
end;

procedure TfrmAVALIACAO.btnCANCELA_FILTRO_SITUACAO_AVALIACAOClick(
  Sender: TObject);
begin
  cmbFILTRO_SITUACAO_AVALIACAO.ItemIndex := -1;
end;

procedure TfrmAVALIACAO.btnEDITAR_AVALIACAOClick(Sender: TObject);
begin
  if (dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ind_situacao').Value = 2) then
  begin
    Application.MessageBox('A AVALIA��O selecionada n�o pode ser mais editada.' +#13+#13+
                           'O status dela encontra-se FINALIZADA!' +#13+#13+
                           'Qualquer d�vida entre em contato com a DECA',
                           '[SAECA] - Per�odo Finalizado',
                           MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    PageControl1.ActivePageIndex := 1;

    //Transportar os dados da AVALIA��O e dos PER�ODOS (ITENS) para c�....
    edMATRICULA.Text                 := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('cod_matricula').Value;
    edNOME.Text                      := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('nom_nome').Value;
    edNASCIMENTO.Text                := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('dat_nascimento').Value;

    //Bloqueia os campos principais e hablita os de avalia��o
    edMATRICULA.Enabled              := False;
    edNOME.Enabled                   := False;
    edNASCIMENTO.Enabled             := False;
    edANO_AVALIACAO.Enabled          := False;
    cmbFASE.Enabled                  := False;
    cmbPERIODO_AVALIACAO.Enabled     := False;


    edANO_AVALIACAO.Text             := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('num_ano').Value;
    lbIDADE.Caption                  := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('idade_completa_atual').Value;

    cmbFASE.ItemIndex                := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ind_fase').Value - 1;
    cmbPERIODO_AVALIACAO.ItemIndex   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ind_periodo').Value;

    cmbCONVIVE1.ItemIndex            := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('convivencia1').Value;
    cmbCONVIVE2.ItemIndex            := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('convivencia2').Value;
    cmbCONVIVE3.ItemIndex            := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('convivencia3').Value;
    cmbCONVIVE4.ItemIndex            := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('convivencia4').Value;

    cmbFLUENCIACOM1.ItemIndex        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('fluenciacom1').Value;
    cmbFLUENCIACOM2.ItemIndex        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('fluenciacom2').Value;
    cmbFLUENCIACOM3.ItemIndex        := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('fluenciacom3').Value;

    cmbFLUMAT1.ItemIndex             := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('fluenciamat1').Value;
    cmbFLUMAT2.ItemIndex             := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('fluenciamat2').Value;
    cmbFLUMAT3.ItemIndex             := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('fluenciamat3').Value;

    cmbPENS_CRITCIENT1.ItemIndex     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('pensamentocrit1').Value;
    cmbPENS_CRITCIENT2.ItemIndex     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('pensamentocrit2').Value;
    cmbPENS_CRITCIENT3.ItemIndex     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('pensamentocrit3').Value;

    cmbAMPLIACAO_SABERES1.ItemIndex  := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ampliacaosaberes1').Value;
    cmbAMPLIACAO_SABERES2.ItemIndex  := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('ampliacaosaberes2').Value;

    cmbINICIACAO_TRAB1.ItemIndex     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('iniciacaomundotrab1').Value;
    cmbINICIACAO_TRAB2.ItemIndex     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('iniciacaomundotrab2').Value;
    cmbINICIACAO_TRAB3.ItemIndex     := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('iniciacaomundotrab3').Value;

    cmbACESSO_SERVBASICOS1.ItemIndex := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('servicosbasicos1').Value;
    cmbACESSO_SERVBASICOS2.ItemIndex := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('servicosbasicos2').Value;
    cmbACESSO_SERVBASICOS3.ItemIndex := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('servicosbasicos3').Value;

    cmbPARTIC_PROTAG1.ItemIndex      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('partprotag1').Value;
    cmbPARTIC_PROTAG2.ItemIndex      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('partprotag2').Value;
    cmbPARTIC_PROTAG3.ItemIndex      := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('partprotag3').Value;

    meCONSIDERACOES.Text             := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('consideracoes').Value;
    rdgATINGIU_OBJETIVOS.ItemIndex   := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('atingiu_objetivos').Value;
    meJUSTIFICATIVA.Text             := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('justificativa').Value;

    PageControl2.ActivePageIndex     := 0;

    //Carrega os profissionais cadastrados para acesso � unidade, selecionados como avaliadores ou n�o...
    try
      with dmDeca.cdsSEL_ACOMPEVOLUCAO_EQUIPE do
      begin
        Close;
        Params.ParamByName('@pe_id_equipe').Value        := Null;
        Params.ParamByName('@pe_id_itemavaliacao').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('id_itemavaliacao').Value;
        Params.ParamByName('@pe_cod_usuario').Value      := Null;
        Open;

        dbgEQUIPE_AVALIADORA.Refresh;

      end;
    except

    end;

  end;

end;

procedure TfrmAVALIACAO.btnEDITAR_PERIODOClick(Sender: TObject);
begin
  if Application.MessageBox ('Deseja manter as ALTERA��ES realizadas na AVALIA��O atual?',
                             '[SAECA] - Confirma lan�amento dos dados da AVALIA��O',
                             MB_YESNO + MB_ICONQUESTION) = idYes then
  begin
    //Criar rotina para validar os itens da avalia��o...


    with dmDeca.cdsUPD_ACOMPEVOLUCAO_ITENS do
    begin
      Close;
      Params.ParamByName('@pe_id_itemavaliacao').Value    := dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS.FieldByName('id_itemavaliacao').Value;
      Params.ParamByName('@pe_ind_situacao').Value        := 1;     //0->GERADA; 1->AVALIADA; 2->FINALIZADA
      Params.ParamByName('@pe_convivencia1').Value        := cmbCONVIVE1.ItemIndex;
      Params.ParamByName('@pe_convivencia2').Value        := cmbCONVIVE2.ItemIndex;
      Params.ParamByName('@pe_convivencia3').Value        := cmbCONVIVE3.ItemIndex;
      Params.ParamByName('@pe_convivencia4').Value        := cmbCONVIVE4.ItemIndex;
      Params.ParamByName('@pe_fluenciacom1').Value        := cmbFLUENCIACOM1.ItemIndex;
      Params.ParamByName('@pe_fluenciacom2').Value        := cmbFLUENCIACOM2.ItemIndex;
      Params.ParamByName('@pe_fluenciacom3').Value        := cmbFLUENCIACOM3.ItemIndex;
      Params.ParamByName('@pe_fluenciamat1').Value        := cmbFLUMAT1.ItemIndex;
      Params.ParamByName('@pe_fluenciamat2').Value        := cmbFLUMAT2.ItemIndex;
      Params.ParamByName('@pe_fluenciamat3').Value        := cmbFLUMAT3.ItemIndex;
      Params.ParamByName('@pe_pensamentocrit1').Value     := cmbPENS_CRITCIENT1.ItemIndex;
      Params.ParamByName('@pe_pensamentocrit2').Value     := cmbPENS_CRITCIENT2.ItemIndex;
      Params.ParamByName('@pe_pensamentocrit3').Value     := cmbPENS_CRITCIENT3.ItemIndex;
      Params.ParamByName('@pe_ampliacaosaberes1').Value   := cmbAMPLIACAO_SABERES1.ItemIndex;
      Params.ParamByName('@pe_ampliacaosaberes2').Value   := cmbAMPLIACAO_SABERES2.ItemIndex;
      Params.ParamByName('@pe_iniciacaomundotrab1').Value := cmbINICIACAO_TRAB1.ItemIndex;
      Params.ParamByName('@pe_iniciacaomundotrab2').Value := cmbINICIACAO_TRAB2.ItemIndex;
      Params.ParamByName('@pe_iniciacaomundotrab3').Value := cmbINICIACAO_TRAB3.ItemIndex;
      Params.ParamByName('@pe_servicosbasicos1').Value    := cmbACESSO_SERVBASICOS1.ItemIndex;
      Params.ParamByName('@pe_servicosbasicos2').Value    := cmbACESSO_SERVBASICOS2.ItemIndex;
      Params.ParamByName('@pe_servicosbasicos3').Value    := cmbACESSO_SERVBASICOS3.ItemIndex;
      Params.ParamByName('@pe_partprotag1').Value         := cmbPARTIC_PROTAG1.ItemIndex;
      Params.ParamByName('@pe_partprotag2').Value         := cmbPARTIC_PROTAG2.ItemIndex;
      Params.ParamByName('@pe_partprotag3').Value         := cmbPARTIC_PROTAG3.ItemIndex;
      Params.ParamByName('@pe_cod_usuario').Value         := vvCOD_USUARIO;
      Params.ParamByName('@pe_consideracoes').Value       := Trim(meCONSIDERACOES.Text);
      Params.ParamByName('@pe_atingiu_objetivos').Value   := rdgATINGIU_OBJETIVOS.ItemIndex;
      Params.ParamByName('@pe_justificativa').Value       := Trim(meJUSTIFICATIVA.Text);
      Execute;
    end;

    //Inserir dados no LOG da Avalia��o...
    with dmDeca.cdsINS_ACOMPEVOLUCAO_LOG do
    begin
      Close;
      Params.ParamByName('@pe_dat_registro').Value   := Date();
      Params.ParamByName('@pe_cod_usuario').Value    := vvCOD_USUARIO;
      Params.ParamByName('@pe_dsc_ocorrencia').Value := 'Avalia��o modificada em ' + DateToStr(Date()) + ' por ' + vvNOM_USUARIO;
      Execute;
    end;

    //Capturar os itens ainda n�o avaliados

    //Atualiza o status da avalia��o referente ao per�odo em tela...
    try
      with dmDeca.cdsUPD_ACOMPEVOLUCAO do
      begin
        Close;
        Params.ParamByName('@pe_id_avaliacao').Value := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
        Params.ParamByName('@pe_ind_situacao').Value := 1; //AVALIADA
        Execute;
      end;
    except

    end;

    //Consulta os per�odos lan�ados para a avalia��o selecionada
    try
      with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
      begin
        Close;
        Params.ParamByName('@pe_id_itemavaliacao').Value := Null;
        Params.ParamByName('@pe_id_avaliacao').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
        Params.ParamByName('@pe_ind_fase').Value         := Null;
        Params.ParamByName('@pe_periodo').Value          := Null;
        Open;
        dbgITENS_AVALIACOES.Refresh;
      end;
    except

    end;

   //Retornar � tela de Avalia��es...
    PageControl1.ActivePageIndex  := 0;
    try
      with dmDeca.cdsSEL_ACOMPEVOLUCAO do
      begin
        Close;
        Params.ParamByName('@pe_id_avaliacao').Value      := Null;
        //Params.ParamByName('@pe_ind_fase').Value          := Null;
        //Params.ParamByName('@pe_ind_periodo').Value       := Null;
        Params.ParamByName('@pe_num_ano').Value           := Null;
        Params.ParamByName('@pe_ind_situacao').Value      := Null;
        Params.ParamByName('@pe_cod_matricula').Value     := Null;
        Params.ParamByName('@pe_cod_unidade').Value       := vvCOD_UNIDADE;
        Params.ParamByName('@pe_dsc_periodo').Value       := Null;
        Params.ParamByName('@pe_nom_nome').Value          := Null;
        Open;
        dbgAVALIACOES.Refresh;
      end;
    except

    end;

  end;
end;

procedure TfrmAVALIACAO.btnFILTRO_ITENSClick(Sender: TObject);
begin
     try
      with dmDeca.cdsSEL_ACOMPEVOLUCAO_itens do
      begin
        Close;
        Params.ParamByName('@pe_id_avaliacao').Value := Null;
        Params.ParamByName('@pe_id_avaliacao').Value := Null;
        Params.ParamByName('@pe_id_avaliacao').Value := Null;
        Params.ParamByName('@pe_id_avaliacao').Value := Null;



        if cmbFILTRO_PERIODO.ItemIndex = -1 then Params.ParamByName('@pe_dsc_periodo').Value := Null else Params.ParamByName('@pe_dsc_periodo').Value := Trim(cmbFILTRO_PERIODO.Text);
        //if cmbFILTRO_FASE_ETARIA.ItemIndex = -1 then Params.ParamByName('@pe_ind_fase').Value := Null else Params.ParamByName('@pe_ind_fase').Value := cmbFILTRO_FASE_ETARIA.ItemIndex+1;
        //if cmbFILTRO_PERIODO_AVALIACAO.ItemIndex = -1 then Params.ParamByName('@pe_ind_periodo').Value := Null else Params.ParamByName('@pe_ind_periodo').Value := cmbFILTRO_PERIODO_AVALIACAO.ItemIndex;
        if cmbFILTRO_SITUACAO_AVALIACAO.ItemIndex = -1 then Params.ParamByName('@pe_ind_situacao').Value := Null else Params.ParamByName('@pe_ind_situacao').Value := cmbFILTRO_SITUACAO_AVALIACAO.ItemIndex;
        Params.ParamByName('@pe_num_ano').Value       := StrToInt(sedFILTRO_ANO_AVALIACAO.Text);
        Params.ParamByName('@pe_cod_unidade').Value   := vvCOD_UNIDADE;
        if (Length(edtFILTRO_NOME.Text)=0) then Params.ParamByName('@pe_nom_nome').Value := Null else Params.ParamByName('@pe_nom_nome').Value := Trim(edtFILTRO_NOME.Text) + '%';

        Open;
        dbgAVALIACOES.Refresh;
      end;
    except

    end;
end;

procedure TfrmAVALIACAO.btnINICIALIZAR_AVALIACAOClick(Sender: TObject);
begin
  Application.CreateForm(TfrmINICIALIZAR_AVALIACAO,frmINICIALIZAR_AVALIACAO);
  frmINICIALIZAR_AVALIACAO.ShowModal;
end;

procedure TfrmAVALIACAO.btnLANCAR_EQUIPEClick(Sender: TObject);
begin
  Application.CreateForm(TfrmCADASTRA_EQUIPE, frmCADASTRA_EQUIPE);
  frmCADASTRA_EQUIPE.ShowModal;
end;

procedure TfrmAVALIACAO.btnNOVA_AVALIACAO2Click(Sender: TObject);
begin

  //Caso tenha selecionado um registro vai para a FICHA DE AVALIA��O, carregando os dados necess�rios...
  if (dbgAVALIACOES.Columns.Grid.Focused) then
  begin
    PageControl1.ActivePageIndex  := 2;

    //Repassa os valores para o "cabe�alho" da Avalia��o
    edMATRICULA.Text       := dmDeca.cdsSEL_CADASTRO.FieldByName('cod_matricula').Value;
    edNOME.Text            := dmDeca.cdsSEL_CADASTRO.FieldByName('nom_nome').Value;
    edNASCIMENTO.Text      := dmDeca.cdsSEL_CADASTRO.FieldByName('dat_nascimento').Value;
    lbIDADE.Caption        := dmDeca.cdsSEL_CADASTRO.FieldByName('idadeAtual').Value;
    edANO_AVALIACAO.Text   := '2019';
  end;

end;

procedure TfrmAVALIACAO.btnSAIRClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmAVALIACAO.dbgAVALIACOESCellClick(Column: TColumn);
begin
  //Consulta os per�odos lan�ados para a avalia��o selecionada
  try
    with dmDeca.cdsSEL_ACOMPEVOLUCAO_ITENS do
    begin
      Close;
      Params.ParamByName('@pe_id_itemavaliacao').Value := Null;
      Params.ParamByName('@pe_id_avaliacao').Value     := dmDeca.cdsSEL_ACOMPEVOLUCAO.FieldByName('id_avaliacao').Value;
      Params.ParamByName('@pe_ind_fase').Value         := Null;
      Params.ParamByName('@pe_periodo').Value          := Null;
      Open;
      dbgITENS_AVALIACOES.Refresh;
    end;
  except

  end;



end;

procedure TfrmAVALIACAO.dbgAVALIACOESDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin

  with dbgAVALIACOES do
  begin

    case dsSEL_ACOMPEVOLUCAO.DataSet.FieldByName('ind_situacao').Value of
      0: begin
           dbgAVALIACOES.Canvas.Font.Color:= clWhite;
           dbgAVALIACOES.Canvas.Brush.Color:= clGreen;
      end;

      1: begin
           dbgAVALIACOES.Canvas.Font.Color:= clRed;
           dbgAVALIACOES.Canvas.Brush.Color:= clWhite;
      end;

      2: begin
           dbgAVALIACOES.Canvas.Font.Color:= clWhite;
           dbgAVALIACOES.Canvas.Brush.Color:= clBlue;
      end;
    end;
  end;

  dbgAVALIACOES.Canvas.FillRect(Rect);
  dbgAVALIACOES.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);

end;

procedure TfrmAVALIACAO.dbgITENS_AVALIACOESDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  with dbgAVALIACOES do
  begin

    case dsSEL_ACOMPEVOLUCAO_ITENS.DataSet.FieldByName('ind_situacao').Value of
      0: begin
           dbgITENS_AVALIACOES.Canvas.Font.Color:= clWhite;
           dbgITENS_AVALIACOES.Canvas.Brush.Color:= clGreen;
      end;

      1: begin
           dbgITENS_AVALIACOES.Canvas.Font.Color:= clRed;
           dbgITENS_AVALIACOES.Canvas.Brush.Color:= clWhite;
      end;

      2: begin
           dbgITENS_AVALIACOES.Canvas.Font.Color:= clWhite;
           dbgITENS_AVALIACOES.Canvas.Brush.Color:= clBlue;
      end;
    end;
  end;

  dbgITENS_AVALIACOES.Canvas.FillRect(Rect);
  dbgITENS_AVALIACOES.Canvas.TextOut(Rect.Left+2,Rect.Top,Column.Field.AsString);
end;

procedure TfrmAVALIACAO.FormCreate(Sender: TObject);
begin
  //AjustaResolucaoAvaliacao;
end;

procedure TfrmAVALIACAO.FormShow(Sender: TObject);
begin

  PageControl1.ActivePageIndex := 0;
  sedFILTRO_ANO_AVALIACAO.Text := '2019';

  try
    with dmDeca.cdsSEL_ACOMPEVOLUCAO do
    begin
      Close;
        Params.ParamByName('@pe_id_avaliacao').Value      := Null;
        Params.ParamByName('@pe_num_ano').Value           := Null;
        Params.ParamByName('@pe_ind_situacao').Value      := Null;
        Params.ParamByName('@pe_cod_matricula').Value     := Null;
        Params.ParamByName('@pe_cod_unidade').Value       := vvCOD_UNIDADE;
        Params.ParamByName('@pe_dsc_periodo').Value       := Null;
        Params.ParamByName('@pe_nom_nome').Value          := Null;
      Open;
      dbgAVALIACOES.Refresh;
    end;
  except

  end;

end;

procedure TfrmAVALIACAO.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  //AllowChange := False;
end;

procedure TfrmAVALIACAO.SpeedButton1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmAVALIACAO.SpeedButton7Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

end.
