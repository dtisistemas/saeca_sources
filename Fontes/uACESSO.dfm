object frmACESSO: TfrmACESSO
  Left = 559
  Top = 378
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsNone
  BorderWidth = 5
  Caption = 'frmACESSO'
  ClientHeight = 304
  ClientWidth = 585
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 9
    Top = 272
    Width = 566
    Height = 15
    Caption = 
      'Desenvolvido pela Divis'#227'o de Tecnologia da Informa'#231#227'o - FUNDHAS ' +
      '  -   Todos os direitos reservados.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBackground
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label9: TLabel
    Left = 130
    Top = 289
    Width = 323
    Height = 15
    Caption = 'Copyright (R) - Todos os direitos reservados.  2003 - 2019'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBackground
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = btnLoginClick
  end
  object Label1: TLabel
    Left = 0
    Top = 83
    Width = 94
    Height = 19
    Alignment = taRightJustify
    Caption = 'MATR'#205'CULA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBackground
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 20
    Top = 111
    Width = 74
    Height = 19
    Alignment = taRightJustify
    Caption = 'UNIDADE'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBackground
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 37
    Top = 136
    Width = 57
    Height = 19
    Alignment = taRightJustify
    Caption = 'SENHA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBackground
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 486
    Top = 0
    Width = 99
    Height = 32
    Caption = 'SAECA'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -27
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label11: TLabel
    Left = 128
    Top = 32
    Width = 457
    Height = 13
    Caption = 
      'Voc'#234' est'#225' utilizando a vers'#227'o 8.2 do Sistema DECA de 16 de AGOST' +
      'O de 2019.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 184
    Top = 241
    Width = 218
    Height = 16
    Cursor = crHandPoint
    Caption = 'Pressione a tecla <ESC> para sair.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBackground
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbIp: TLabel
    Left = 575
    Top = 48
    Width = 6
    Height = 13
    Alignment = taRightJustify
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object btnLogin: TSpeedButton
    Left = 175
    Top = 180
    Width = 235
    Height = 47
    Caption = 'ACESSO AO SISTEMA'
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = btnLoginClick
  end
  object mskSenha: TMaskEdit
    Left = 100
    Top = 133
    Width = 116
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 10
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 2
    Text = ''
    OnEnter = mskSenhaEnter
    OnExit = mskSenhaExit
  end
  object cbUnidades: TComboBox
    Left = 100
    Top = 107
    Width = 485
    Height = 22
    Style = csOwnerDrawFixed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = cbUnidadesClick
  end
  object mskMatricula: TMaskEdit
    Left = 100
    Top = 81
    Width = 116
    Height = 24
    EditMask = '99999999'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 8
    ParentFont = False
    TabOrder = 0
    Text = '        '
    OnEnter = mskMatriculaEnter
    OnExit = mskMatriculaExit
  end
end
