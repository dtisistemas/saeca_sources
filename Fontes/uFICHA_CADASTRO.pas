unit uFICHA_CADASTRO;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TfrmFICHA_CADASTRO = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    DBGrid1: TDBGrid;
    dsSEL_CADASTRO: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFICHA_CADASTRO: TfrmFICHA_CADASTRO;

implementation

{$R *.dfm}

uses uDmDeca, uPRINCIPAL;

procedure TfrmFICHA_CADASTRO.DBGrid1TitleClick(Column: TColumn);
begin
  dmDeca.cdsSEL_CADASTRO.IndexFieldNames := Column.FieldName;
end;

procedure TfrmFICHA_CADASTRO.FormShow(Sender: TObject);
begin
  try
    with dmDeca.cdsSEL_CADASTRO do
    begin
      Close;
      Params.ParamByName('@pe_cod_matricula').Value := Null;
      Params.ParamByName('@pe_cod_unidade').Value := vvCOD_UNIDADE;
      Open;
    end;
  except

  end;
end;

end.
