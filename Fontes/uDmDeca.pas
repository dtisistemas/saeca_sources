unit uDmDeca;

interface

uses
  System.SysUtils, System.Classes, Datasnap.Win.MConnect, Datasnap.Win.ObjBrkr,
  Data.DB, Datasnap.DBClient, Datasnap.Win.SConnect, Midas, MidasLib, frxChart,
  frxBarcode, frxClass, frxDBSet;

type
  TdmDeca = class(TDataModule)
    socketConn: TSocketConnection;
    SimpleObjectBroker1: TSimpleObjectBroker;
    cdsSEL_USUARIO: TClientDataSet;
    cdsSEL_ACESSO: TClientDataSet;
    cdsSEL_VERSAO: TClientDataSet;
    cdsSEL_CADASTRO: TClientDataSet;
    cdsSEL_ACOMPEVOLUCAO: TClientDataSet;
    cdsINS_ACOMPEVOLUCAO: TClientDataSet;
    cdsINS_ACOMPEVOLUCAO_ITENS: TClientDataSet;
    cdsSEL_ACOMPEVOLUCAO_ITENS: TClientDataSet;
    cdsUPD_ACOMPEVOLUCAO_ITENS: TClientDataSet;
    cdsUPD_ACOMPEVOLUCAO: TClientDataSet;
    cdsSEL_ACOMPEVOLUCAO_EQUIPE: TClientDataSet;
    cdsINS_ACOMPEVOLUCAO_EQUIPE: TClientDataSet;
    cdsDEL_ACOMPEVOLUCAO_EQUIPE: TClientDataSet;
    cdsFINALIZA_PERIODO: TClientDataSet;
    relAVALIACAO: TfrxReport;
    frxDB_ACOMPEVOLUCAO: TfrxDBDataset;
    cdsSEL_ACOMPEVOLUCAO_LOG: TClientDataSet;
    cdsINS_ACOMPEVOLUCAO_LOG: TClientDataSet;
    relRESUMO_AVALIACAO: TfrxReport;
    cdsSEL_ACOMPEVOLUCAO_RESUMO: TClientDataSet;
    cdsINS_AcompEvolucao_Resumo: TClientDataSet;
    cdsDEL_ACOMPEVOLUCAO_RESUMO: TClientDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmDeca: TdmDeca;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uPRINCIPAL;

{$R *.dfm}

end.
