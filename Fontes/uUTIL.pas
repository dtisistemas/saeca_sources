unit uUtil;

interface
  uses stdctrls,mask,Sysutils,dbclient,comctrls;

type
   TStatus = (fdInsert,fdEdit,fdDelete,fdBack,fdNone);

type
   TValueType = (vtNone,vtString,vtInteger,vtReal,vtDate);

  function GetValue(AEdit:TEdit):Variant;overload;
  function GetValue(AEdit:TEdit;AType:TValueType):Variant;overload;
  function GetValue(AEdit:TMaskEdit;AValue:String):Variant;overload;
  function GetValue(AEdit:TMaskEdit;AType:TValueType):Variant;overload;
  function GetValue(AEdit:TComboBox):Variant;overload;
  function GetValue(AEdit:TRichEdit):Variant;overload;
  function GetValue(AEdit:TMemo):Variant;overload;
  function GetValue(AEdit:String):Variant;overload;
  procedure SetDataRefresh(var ASource :TClientDataSet; var ATarget :TClientDataSet;ASql:String);
  function Divisao(Matricula : String): String;
var
  vSQL:String;

const
  cMeses : array[1..12] of String =('Janeiro','Fevereiro','Mar�o','Abril',
                                    'Maio','Junho','Julho','Agosto',
                                    'Setembro','Outubro','Novembro','Dezembro');
implementation

uses uPrincipal, Variants, uDmDeca;

function GetValue(AEdit:TEdit):Variant;
begin
   if trim(AEdit.Text) = '' then
   begin
      Result := NULL;
      Exit;
   end;

   Result :=AEdit.Text;
end;

function GetValue(AEdit:String):Variant;
begin
   if trim(AEdit) = '' then
   begin
      Result := NULL;
      Exit;
   end;

   Result := trim(AEdit);
end;

function GetValue(AEdit:TMaskEdit;AValue:String):Variant;
begin
   if AEdit.Text = AValue then
   begin
      Result := NULL;
      Exit;
   end;

   Result := trim(AEdit.Text);
end;

function GetValue(AEdit:TRichEdit):Variant;
begin
   if AEdit.Text = '' then
   begin
      Result := NULL;
      Exit;
   end;

   Result :=trim(AEdit.Text);
end;

function GetValue(AEdit:TComboBox):Variant;
begin
  if trim(AEdit.Text) = '' then
   begin
      Result:=NULL;
      Exit;
   end;

   Result :=trim(AEdit.Text);
end;

function GetValue(AEdit:TMemo):Variant;
begin
  if trim(AEdit.Text) = '' then
   begin
      Result:=NULL;
      Exit;
   end;

   Result :=trim(AEdit.Text);
end;

function GetValue(AEdit:TMaskEdit;AType:TValueType):Variant;
begin
  if trim(AEdit.Text ) <> '' then
  begin
     case Ord(AType) of
     1: //-- strings
        begin
          Result:=Trim(AEdit.Text);
          Exit;
        end;
     2: //-- Inteiros
        begin
          Result:= strToIntDef(Trim(AEdit.Text),0);
          Exit;
        end;
     3: //-- real
        begin
          try
             Result:=strToFloat(Trim(AEdit.Text));
             Exit;
          except
             Result:= Null;
             Exit;
          end;
        end;
     4: //-- Date
       begin
         try
             Result:=strToDateTime(AEdit.Text);
             Exit;
          except
             Result:=Null;
             Exit;
          end;
        end;
     end;

  end;
  Result:=Null;
end;

function GetValue(AEdit:TEdit;AType:TValueType):Variant;
begin
  if trim(AEdit.Text ) <> '' then
  begin
     case Ord(AType) of
     1: //-- strings
        begin
          Result:=Trim(AEdit.Text);
          Exit;
        end;
     2: //-- Inteiros
        begin
          Result:= strToIntDef(AEdit.Text,0);
          Exit;
        end;
     3: //-- real
        begin
          try
             Result:=strToFloat(AEdit.Text);
             Exit;
          except
             Result:= Null;
             Exit;
          end;
        end;
     4: //-- Date
       begin
         try
             Result:=strToDateTime(AEdit.Text);
             Exit;
          except
             Result:=Null;
             Exit;
          end;
        end;
     end;

  end;
  Result:=Null;
end;

procedure SetDataRefresh(var ASource :TClientDataSet; var ATarget :TClientDataSet;ASql:String);
begin
   try
     ASource.Close;
     ASource.CommandText:= ASql;
     ASource.Open;
     ATarget.Data:= ASource.Data;
     ASource.Close;
   except end;
end;

function Divisao(Matricula : String): String;
begin
  if (Copy(Matricula, 1, 3) = '002') or (Copy(Matricula, 1, 3) = '999') or (Copy(Matricula, 1, 3) = '777') then
    Divisao := 'DIVISAO REGIONAL 2'
  else if (Copy(Matricula, 1, 3) = '003') then
    Divisao := 'DIVISAO REGIONAL 1'
  else if (Copy(Matricula, 1, 3) = '006') then
    Divisao := 'DIVISAO DE EMPREGABILIDADE';
end;

end.
