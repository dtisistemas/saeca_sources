program SAECA;

uses
  Vcl.Forms,
  uPRINCIPAL in 'uPRINCIPAL.pas' {frmPRINCIPAL},
  Vcl.Themes,
  Vcl.Styles,
  uAVALIACAO in 'uAVALIACAO.pas' {frmAVALIACAO},
  uACESSO in 'uACESSO.pas' {frmACESSO},
  uDmDeca in 'uDmDeca.pas' {dmDeca: TDataModule},
  uUTIL in 'uUTIL.pas',
  uINICIALIZAR_AVALIACAO in 'uINICIALIZAR_AVALIACAO.pas' {frmINICIALIZAR_AVALIACAO},
  uCADASTRA_EQUIPE in 'uCADASTRA_EQUIPE.pas' {frmCADASTRA_EQUIPE},
  uGERARDADOS_DRIVE in 'uGERARDADOS_DRIVE.pas' {frmGERARDADOS_DRIVE};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Cyan Dusk');
  Application.CreateForm(TdmDeca, dmDeca);
  Application.CreateForm(TfrmACESSO, frmACESSO);
  Application.Run;
end.
